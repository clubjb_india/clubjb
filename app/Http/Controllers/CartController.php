<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;
use Cart;
class CartController extends Controller
{
    public function getClient()
    {
        $token  =Session::get('token');
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];
        return $client = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers] );
    }

    public function dealDetails(Request $request)
    {
        $input = $request->input();
        if(count($input) > 0){
            $dealId  = $input['dealId'];
            unset($input['dealId']);
            unset($input['_token']);
            $client = $this->getClient();
            $dealGroupResponse = $client->request('POST', 'storedealbycategory',  [
                'form_params' => ['param'=>json_encode($input)],
            ]);
            $deals = json_decode((string) $dealGroupResponse->getBody())->data;
            $data['dealDetail'] = [];
            foreach($deals as $deal){
                if($deal->id == $dealId){
                    $data['dealDetail'] = json_decode(json_encode($deal), true);
                    break;
                }
            }
dd($data['dealDetail']);
            /*if(count($dealDetail) > 0){
                Cart::add(['id' => $dealId, 'name' => $dealDetail['deal_name'], 'qty' => $dealDetail['quantity'], 'price' => $dealDetail['final_price'],'weight'=>'', 'options' => $dealDetail]);
            }*/
        }

        return view('deal-buy', $data);
    }

    public function removeItemFromCart($rowId)
    {
        Cart::remove($rowId);
    }

    public function emptyCart()
    {
        Cart::destroy();
    }
}
