<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Country;
use Session;
use Illuminate\Support\Facades\Input;

class MembershipController extends Controller
{

    public function getClient()
    {
        $token  =Session::get('token');
        //$token  = \Cache::get('token');
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            //'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];
        return $client = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers] );
    }

    public function register()
    {
        $user = $this->getUser();
        if($user != null){
            return redirect('membership-price-group');
        }
        $countries = Country::select('id', 'country_code', 'country_name')->get();
        $data['countries'] = $countries;
        $data['input']['country_id'] = '';
        $headers = [
            'Accept'        => 'application/json',
        ];
        $client = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers]);
        $termsResponse = $client->request('GET', 'page/details/clubjb_term_condition');
        $data['terms']  = json_decode((string) $termsResponse->getBody())->result;
        $privacyResponse = $client->request('GET', 'page/details/clubjb_privacy_policy');
        $data['privacy_policy']  = json_decode((string) $privacyResponse->getBody())->result;
        return view('login-signup', $data);
        //return view('register', $data);
    }

    public function verify_otp()
    {
        $input['phone_number'] = Input::get('phone_number');
        $input['otp_code'] = Input::get('otp_code');
        $headers = [
            'Accept'        => 'application/json',
        ];
        $client = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers]);
        $responseOTP = $client->request('POST', 'verify_otp_web', [
            'form_params' => $input
        ]);
        $response = json_decode((string)  $responseOTP->getBody());
        if($response->status == 200){
            $token = $response->result->token;
            Session::put('token', $token);
            Session::save();
            //\Cache::put('token', $token);
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept'        => 'application/json',
            ];
            $clientNew = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers] );
            $response1 = $clientNew->request('POST', 'profile_web');
            $userData = json_decode((string) $response1->getBody())->result;
            Session::put('authUser', $userData);
            Session::save();
            //\Cache::put('authUser', $userData);
       }
        return \Response::json($response);
        //return json_encode($response);
    }

    /*public function register_and_login(Request $request)
    {
        $input = $request->all();
        unset($input['_token']);
        $rules = [
            'country_id' => 'required|numeric',
            'first_name' => 'required',
            'phone_number' => 'required|digits:10',
            'password' => 'required|string|min:6',
        ];
        $this->validate($request, $rules);
        $headers = [
            'Accept'        => 'application/json',
        ];
        $client = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers]);
        $response = $client->request('POST', 'register_web', [
            'form_params' => $input
        ]);
        $response = json_decode((string)  $response->getBody());
        dd($response);
        if($response->status == 200){
            $token = $response->result->original->access_token;
            Session::put('token', $token);

            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept'        => 'application/json',
            ];
            $clientNew = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers] );
            $response1 = $clientNew->request('POST', 'profile_web');
            Session::put('authUser', json_decode((string) $response1->getBody())->result);
            return redirect('membership-price-group');
        }else{
            return redirect()->back()->withErrors(['errors'=>$response->message])->withInput($input);
        }

    }*/

    public function logout()
    {
        $client = $this->getClient();
        $response = $client->request('POST', 'logout_web');
        $status = json_decode((string) $response->getBody())->status;
        if($status == 200){
           // Cache::forget('token');
            //Cache::forget('authUser');
            Session::flush();
        }
        return redirect('/register');

    }

    public function update_user(Request $request)
    {
        $input = $request->all();
        $client = $this->getClient();
        $response = $client->request('POST', 'update-user', [
            'form_params' => [
                'city_id' => $input['city_id'],
                'state_id' => $input['state_id']
            ]
        ]);
        $status = json_decode((string) $response->getBody())->status;
        if($status == 201){
             $clientNew = $this->getClient();
            $response1 = $clientNew->request('POST', 'profile_web');
            $userData = json_decode((string) $response1->getBody())->result;
            Session::put('authUser', $userData);
           // \Cache::put('authUser', $userData);
        }
       return redirect('membership-price-group');
    }

    public function dashboard()
    {
        $countryId = 101;
        $stateId = 32;
        $client = $this->getClient();
        $citiesResponse = $client->request('GET', 'city/'.$countryId.'/'.$stateId);
        //$body = $citiesResponse->getBody();
        $locations  = json_decode((string) $citiesResponse->getBody());
        $data['locations'] = [];
        foreach($locations->result as $location){
            $data['locations'][$location->id] = $location->city_name;
        }
        $termsResponse = $client->request('GET', 'page/details/clubjb_term_condition');
        $data['terms']  = json_decode((string) $termsResponse->getBody())->result;
        return view('dashboard', $data);
    }

    public function membershipPriceGroups()
    {
        $user = $this->getUser();
        if($user == null){
            return redirect('register');
        }
        if($user->city_id == null){
            return redirect('location');
        }
        //dd($user);
        $client = $this->getClient();
        $data['cities'] = [''=>''];
        $data['states'] = $this->getStates($user->country_id);
        if($user->state_id !=null){
            $data['cities'] = $this->getCities($user->country_id, $user->state_id);
        }
        $response = $client->request('POST', 'membershippricegroup', [
            'form_params' => [
                'city_Id' => $user->city_id,
            ]
        ]);
        $data['user'] = $user;
        $memPriceGroups = json_decode((string) $response->getBody());
        $data['memPriceGroups'] = $memPriceGroups->result;
        return view('membership_price_group', $data);
    }

    public function membershipList($price)
    {
        $user = $this->getUser();
        /*if($user == null){
            return redirect('register');
        }
        if($user->city_id == null){
            return redirect('location');
        }*/
        $data['cities'] = [''=>''];
        $data['states'] = $this->getStates($user->country_id);
        if($user->state_id !=null){
            $data['cities'] = $this->getCities($user->country_id, $user->state_id);
        }
        $data['user'] = $user;
        $client = $this->getClient();
        $response = $client->request('POST', 'memberships', [
            'form_params' => [
                'keyword' => 'KEYWORD',
                'city_id' => $user->city_id,
                'page' => 1,
                'type' =>'CITY_MEMBERSHIP',
                'price_group'=>$price
            ]
        ]);
       $data['cityMemberships'] = json_decode((string) $response->getBody());
        $response = $client->request('POST', 'memberships', [
            'form_params' => [
                'keyword' => 'KEYWORD',
                'city_id' => $user->city_id,
                'page' => 1,
                'type' =>'USER_MEMBERSHIP',
                'price_group'=>''
            ]
        ]);
        $data['purchasedMemberships'] = json_decode((string) $response->getBody());
        return view('membership-list', $data);
    }

    public function storeCategories($membershipId)
    {
        $user = $this->getUser();
        if($user == null){
            return redirect('register');
        }
        $data['user'] = $user;
        $client = $this->getClient();
        $response = $client->request('POST', 'getallstorecategories', [
            'form_params' => [
                'city_Id' => $user->city_id,
                'membership_Id' => $membershipId,
            ]
        ]);
        $data['categories'] = json_decode((string) $response->getBody())->result;
        return view('store-categories', $data, compact('membershipId'));
    }

    public function membershipStores($membershipId, $categoryId)
    {
        $user = $this->getUser();
        if($user == null){
            return redirect('register');
        }
        $data['user'] = $user;
        $client = $this->getClient();
        $storeResponse = $client->request('POST', 'store', [
            'form_params' => ['param'=>json_encode([
                'category_id' => $categoryId,
                'city_id' => $user->city_id,
                'membership_id' => $membershipId,
                'type'=>'MEMBERSHIP_STORE'
            ])],
        ]);
        $data['allStores'] = json_decode((string) $storeResponse->getBody())->result;
        return view('membership-store', $data, compact('membershipId'));
    }

    public function storeDealGroup($membershipId,$storeId)
    {
        $user = $this->getUser();
        if($user == null){
            return redirect('register');
        }
        $data['user'] = $user;
        $client = $this->getClient();
        $dealGroupResponse = $client->request('POST', 'storecategorylist',  [
            'form_params' => ['param'=>json_encode([
                'store_id' => $storeId,
                'membership_id' => $membershipId
            ])],

        ]);
        $data['dealGroups'] = json_decode((string) $dealGroupResponse->getBody())->result;
        return view('deals-group', $data, compact('membershipId', 'storeId'));
    }

    public function storeDealByCategory($membershipId, $storeId, $cat_type)
    {
        $user = $this->getUser();
        if($user == null){
            return redirect('register');
        }
        $data['user'] = $user;
        $client = $this->getClient();
        $dealGroupResponse = $client->request('POST', 'storedealbycategory',  [
            'form_params' => ['param'=>json_encode([
                'cat_type' => $cat_type,
                'store_id' => $storeId,
                'membership_id' => $membershipId
            ])],
        ]);
        $data['deals'] = json_decode((string) $dealGroupResponse->getBody());

        $data['purchased'] = $this->checkIfPurchased($user->city_id, $membershipId);
        return view('store-deals-bycategory', $data, compact('membershipId','storeId', 'cat_type'));
    }

    public function dealDetails($membershipId, $storeId, $cat_type, $dealId)
    {
        $user = $this->getUser();
        if($user == null){
            return redirect('register');
        }
        $data['user'] = $user;
            $client = $this->getClient();
            $dealGroupResponse = $client->request('POST', 'storedealbycategory',  [
                'form_params' => ['param'=>json_encode([
                    'cat_type' => $cat_type,
                    'store_id' => $storeId,
                    'membership_id' => $membershipId
                ])],
            ]);
            $deals = json_decode((string) $dealGroupResponse->getBody())->data;
            $data['dealDetail'] = [];
            foreach($deals as $deal){
                if($deal->id == $dealId){
                    $data['dealDetail'] = json_decode(json_encode($deal), true);
                    break;
                }
            }
        $data['purchased'] = $this->checkIfPurchased($user->city_id, $membershipId);
       return view('deal-details', $data, compact('membershipId' ));
    }

    public function checkIfPurchased($cityId, $membershipId)
    {
        $client = $this->getClient();
        $response = $client->request('POST', 'memberships', [
            'form_params' => [
                'keyword' => 'KEYWORD',
                'city_id' => $cityId,
                'page' => 1,
                'type' =>'USER_MEMBERSHIP',
                'price_group'=>''
            ]
        ]);
        $purchasedMemberships = json_decode((string) $response->getBody())->data;
        $purchased = false;
        foreach($purchasedMemberships as $purchasedMembership){
            if($purchasedMembership->id == $membershipId){
                return $purchased = true;
            }
        }
        return $purchased;
    }

    public function location()
    {
        $user = $this->getUser();
        // dd($user);
        if($user == null){
            return redirect('register');
        }

        $data['states'] = [''=>''];
        $data['cities'] = [''=>''];
        $data['states'] = $this->getStates($user->country_id);
        if($user->state_id !=null){
            $data['cities'] = $this->getCities($user->country_id, $user->state_id);
        }
        $data['user'] = $user;
        return view('location', $data);
    }


    public function getCities($countryId, $stateId)
    {
        $client = $this->getClient();
        $citiesResponse = $client->request('GET', 'city/'.$countryId.'/'.$stateId);
        $cities = json_decode((string) $citiesResponse->getBody())->result;
        $cityList = [];
        foreach($cities as $city){
            $cityList[$city->id] = $city->city_name;
        }
        return $cityList;
    }

    public function getStates($countryId)
    {
        $client = $this->getClient();
        $statesResponse = $client->request('GET', 'state/'.$countryId);
        $states = json_decode((string) $statesResponse->getBody())->result;
        $stateList = [];
        foreach($states as $state){
            $stateList[$state->id] = $state->state_name;
        }
        return $stateList;
    }

    public function getUser()
    {
        //return $user = \Cache::get('authUser');
        return $user = Session::get('authUser');
    }
    public function allMemberships()
    {

    }

    public function allCategories()
    {
        
    }

    public function store()
    {
        $client = $this->getClient();
        $response = $client->request('POST', 'store', [
            'params' => [
                'category_id' => 25,
                'city_id' => 3203,
                'membership_id' => 229,
            ]
        ]);
        dd($response);
        $body = $response->getBody();
        print_r(json_decode((string) $body));
    }


    public function setFlash()
    {
     //   session_start();
       // \Cache::put('message', 'cache test');
        ini_set('display_errors', 1);
        $_SESSION['php_session'] = 'Nisha';
        Session::flash('message', 'flash message');
        session()->flash('status', 'Task was successful!');
        return redirect('getFlash');
    }

    public function getFlash()
    {
        ini_set('display_errors', 1);
        print_r(Session::all());
        echo session('status');
        echo Session::get('message');
    }
}
