<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Softon\Indipay\Facades\Indipay;
use GuzzleHttp\Client;
use Session;
class PaymentController extends Controller
{

    public function getClient()
    {
        $token  =Session::get('token');
        //$token  = \Cache::get('token');
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];
        return $client = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers] );
    }

    public function orderMembership(Request $request)
    {
        $input = $request->all();
        $client = $this->getClient();
        $response = $client->request('POST', 'order-membership', [
            'form_params' => [
                'membership_id' => $input['membership_id'],
                'quantity' => $input['quantity'],
                'is_coin_used' => $input['is_coin_used']
            ]
        ]);
        $metaData = json_decode((string) $response->getBody());
        $parameters = [
            'tid' => '1',
            'order_id' =>$metaData->data->order_id,
            'amount' => $metaData->data->amount,
        ];

        $order = Indipay::prepare($parameters);
        return Indipay::process($order);

    }
}
