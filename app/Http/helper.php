<?php
use GuzzleHttp\Client;

function if_purchased_memberships($cityId, $membershipId){
    $token  = \Session::get('token');
    //$token  = \Cache::get('token');
    $headers = [
        'Authorization' => 'Bearer ' . $token,
        //'Authorization' => 'Bearer ' . $token,
        'Accept'        => 'application/json',
    ];
    $client = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers] );
    $response = $client->request('POST', 'memberships', [
        'form_params' => [
            'keyword' => 'KEYWORD',
            'city_id' => $cityId,
            'page' => 1,
            'type' =>'USER_MEMBERSHIP',
            'price_group'=>''
        ]
    ]);
    $purchasedMemberships = json_decode((string) $response->getBody())->data;
    foreach($purchasedMemberships as $purchasedMembership){
        if($purchasedMembership->id == $membershipId){
            return true;
        }
    }

}

function membershipDetails($cityId, $membershipId){
    //$token  = \Cache::get('token');
    $token  = \Session::get('token');
    $headers = [
        'Authorization' => 'Bearer ' . $token, //'Authorization' => 'Bearer ' . $token,
        'Accept'        => 'application/json',
    ];
    $client = new Client(['base_uri' => env('API_HOST'), 'headers' => $headers] );
    $response = $client->request('POST', 'memberships', [
        'form_params' => [
            'keyword' => 'KEYWORD',
            'city_id' => $cityId,
            'page' => 1,
            'type' =>'CITY_MEMBERSHIP',
            'price_group'=>''
        ]
    ]);
    $memberships = json_decode((string) $response->getBody())->data;
    foreach($memberships as $membershipDetail){
        if($membershipDetail->id == $membershipId){
            return $membershipDetail;
        }
    }
}

function getCatType($dealGroupName){
    $cat_type = $dealGroupName;
    if($dealGroupName == 'Exclusive Deals'){
        $cat_type = 'is_special_combo';
    }else if($dealGroupName == 'Buy 1 + Get 1 Deals'){
        $cat_type = 'is_buy_1_get_1';
    }else if($dealGroupName == '50% Off On Second'){
        $cat_type = 'is_50_percent_off_on_second';
    }else if($dealGroupName == 'Buy 2 + Get 1 Deals'){
        $cat_type = 'is_buy_2_get_1';
    }else if($dealGroupName == 'Food Deals'){
        $cat_type = 'is_food';
    }else if($dealGroupName == 'Flat Rupees Off Deals'){
        $cat_type = 'is_flat_rupee_off';
    }else if($dealGroupName == 'Percentage(%) Off Deals'){
        $cat_type = 'is_percent_off';
    }else if($dealGroupName == 'Combo Deals'){
        $cat_type = 'is_combo';
    }else if($dealGroupName == 'Buy 3 + Get 1 Deals'){
        $cat_type = 'is_buy_3_get_1';
    }else if($dealGroupName == 'Freebies'){
        $cat_type = 'is_free_bies';
    }else if($dealGroupName == 'Other Deals'){
        $cat_type = 'is_other';
    }
    return $cat_type;
}

function getDealGroupName($cat_type){
    $dealGroupName = $cat_type;
    if($cat_type == 'is_special_combo'){
        $dealGroupName = 'Exclusive Deals';
    }else if($cat_type == 'is_buy_1_get_1'){
        $dealGroupName = 'Buy 1 + Get 1 Deals';
    }else if($cat_type == 'is_50_percent_off_on_second'){
        $dealGroupName = '50% Off On Second';
    }else if($cat_type == 'is_buy_2_get_1'){
        $dealGroupName = 'Buy 2 + Get 1 Deals';
    }else if($cat_type == 'is_food'){
        $dealGroupName = 'Food Deals';
    }else if($cat_type == 'is_flat_rupee_off'){
        $dealGroupName = 'Flat Rupees Off Deals';
    }else if($cat_type == 'is_percent_off'){
        $dealGroupName = 'Percentage(%) Off Deals';
    }else if($cat_type == 'is_combo'){
        $dealGroupName = 'Combo Deals';
    }else if($cat_type == 'is_buy_3_get_1'){
        $dealGroupName = 'Buy 3 + Get 1 Deals';
    }else if($cat_type == 'is_free_bies'){
        $dealGroupName = 'Freebies';
    }else if($cat_type == 'is_other'){
        $dealGroupName = 'Other Deals';
    }
    return $dealGroupName;
}