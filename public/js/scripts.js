var API_URL ='https://newjb.clubjb.com/api/v1/';
function fetchStates(event)
{
    var country_id = $('#model_country').val();
    var stateInput = event.data.target;
    var options = '<option></option>';
    $.ajax({
        url : API_URL+'state/'+country_id,
        type: 'get',
        success : function(response){
            var states = JSON.stringify(response.result);
            var states = JSON.parse(states);
            $.each(states,function(id,val){
                options += "<option value='"+val.id+"'>"+val.state_name+"</option>";
            })
            console.log(options);
        },
        complete: function(){
            stateInput.html(options);
            stateInput.select2();
        }

    });
}

function fetchCities(event)
{
    var country_id = $('#model_country').val();
    var state_id = $(this).val();
    var cityInput = event.data.target;
    var options = '<option></option>';
    $.ajax({
        url : API_URL+'city/'+country_id+'/'+state_id,
        type: 'get',
        success : function(response){
            var locations1 = JSON.stringify(response.result);
            var locations = JSON.parse(locations1);
            $.each(locations,function(id,val){
                options += "<option value='"+val.id+"'>"+val.city_name+"</option>";
            })
        },
        complete: function(){
            cityInput.html(options);
            cityInput.select2();
        }

    });
}

function submitCity(){
    $('#model_states').removeClass('red-border');
    $('#model_cities').removeClass('red-border');
    var state= $('#model_states').val();
    var city= $('#model_cities').val();
    if(state == ''){
        $('#model_states').addClass('red-border');
        return false;
    }
    if(city == ''){
        $('#model_cities').addClass('red-border');
        return false;
    }
    $('#model_city').submit();
}

var token  = $('#user_token').val();

function redeemMembership(){
    var membership_id = $('#membership_id').val();
    var city_id = $('#city_id').val();
    var membership_code = $('#membership_code').val();
    $('#success').html('');
    $('#errors').html('');
    if(membership_code == ''){
        var error = '<div class="col-12">The membership code field is required.</div>';
        $('#errors').html(error);

        return false;
    }
    var form = JSON.stringify({'city_id': city_id, 'membership_id': membership_id, 'membership_code': membership_code});
    $.ajax({
        type: 'POST',
        url: API_URL+"activate-membership",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: true,
        crossDomain: true,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+ token
        },
        data: form,
        success: function(response) {
            if(response.status == 406){
                var error = '';
                $.each(response.message, function( index, value ) {
                    error =  error + '<div class="col-12">'+value
                });
                $('#errors').html(error);location.reload();
            }else if(response.status == 400){
                var error =  '<div class="col-12">'+response.message
                '</div>';
                $('#errors').html(error);
            }else{
                var message = '<div class="col-12">'+response.message
                '</div>';
                $('#success').html(message);
                location.reload();
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}