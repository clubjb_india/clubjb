@extends('shared.master')
@section('content')
    <div class="cart-btm">
        <div class="deals-cart">
            <div class="row">
                <div class="col-12">
                    <div class="main-img">
                        <input type="hidden" id="store_deal_id" value="{{$dealDetail['id']}}">
                        <img src="{{$dealDetail['deal_image']}}" alt="" class="img-thumbnail text-center">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-9 pl-5 pr-5">
                                <div class="dealname-txt">
                                    <h5 class="deal-name">{{$dealDetail['deal_name']}}</h5>
                                </div>
                            </div>
                            <div class="col-3 pl-5 pr-5">
                                <div class="icon-circle">
                                    <a href="#"><i class="fa fa-share-alt clr-orange"></i></a>
                                    <a href="#"><i class="fa fa-heart-o"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 pl-5 pr-5">
                                <div class="storename-txt">
                                    <span class="text-black-50 mb-0">Valid For {{$dealDetail['number_of_days']}} Days</span>
                                    <a href="#">{{$dealDetail['store_name']}} <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                            <div class="col-6 pl-5 pr-5">
                                <div class="price-txt pull-right">
                                    <p><b class="short-txt">Actual Deal Price
                                        </b> <span>{{$dealDetail['actual_price']}}/-</span> <i
                                                class="fa fa-rupee"></i> {{$dealDetail['final_price']}}/-</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-5 pl-5 pr-5">
                                <div class="features-txt">
                                    <img src="{{url('images/gold-icon.png')}}" alt="" class="img-thumbnail">
                                    <span class="jb-ratings">
                                      <img src="{{url('images/logo-sm.png')}}" alt="" class="img-thumbnail">
                                      <b>5.0</b>
                                    </span>
                                </div>
                            </div>
                            <div class="col-7 pl-5 pr-5">
                                <div class="buynow-txt pull-right mt-1">
                                    <span>Savings</span>
                                    @if($dealDetail['discount_type'] == 1)<i class="fa fa-rupee"></i> {{$dealDetail['discount_price']}}/- @elseif($dealDetail['discount_type'] == 2) {{$dealDetail['discount_price']}}%@endif
                               </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-6 pl-5 pr-5">
                                <div class="bought-txt">
                                    <p> 500+ Bought
                                        <a href="#">Qty Left:{{$dealDetail['quantity']}}</a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-6 pl-5 pr-5">
                                <div class="ratings-star">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="hr-border0">
                            <hr/>
                        </div>
                        <div class="row">
                            <div class="col-12 pad-0">
                                <div class="deals-tabs">
                                    <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                                        <li class="nav-item active">
                                            <a class="nav-link" id="address-tab" data-toggle="tab" href="#address" role="tab"
                                               aria-controls="address" aria-selected="false">Address</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="terms-tab" data-toggle="tab" href="#terms" role="tab"
                                               aria-controls="terms" aria-selected="false">T&C</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="menu-pics-tab" data-toggle="tab" href="#menu-pics"
                                               role="tab" aria-controls="menu-pics" aria-selected="false">Menus & Pics</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="help-tab" data-toggle="tab" href="#help" role="tab"
                                               aria-controls="help" aria-selected="false">Help</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="google-map-tab" data-toggle="tab" href="#google-map"
                                               role="tab" aria-controls="googl-map" aria-selected="false">Google Map</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="deals-tab-content">
            <div class="row">
                <div class="col-12 pr-5 pl-5">
                    <div class="container">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="address" role="tabpanel" aria-labelledby="address-tab">
                                <div class="address-txt">
                                    <h5><i class="fa fa-home"></i> Address</h5>
                                    <p>
                                         @if($dealDetail['address1'] == null and $dealDetail['address2'] == null and $dealDetail['address3'] == null)
                                            N/A
                                         @else
                                            {{$dealDetail['address1'] .' '. $dealDetail['address2']. ' '.$dealDetail['address3']}}
                                         @endif
                                    </p>
                                    <h5><i class="fa fa-phone"></i> Mobile</h5>
                                    <p>{{$dealDetail['contacts']}}</p>
                                    <h5><i class="fa fa-envelope-o"></i> Email</h5>
                                    <p>{{$dealDetail['email']}}</p>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="terms" role="tabpanel" aria-labelledby="terms-tab">
                                <div class="terms-txt">
                                    {!! $dealDetail['term_condition']!!}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="menu-pics" role="tabpanel" aria-labelledby="menu-pics-tab">
                                <div class="terms-txt">
                                    <p class="text-center text-black-50 mt-4">No data found!</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="help" role="tabpanel" aria-labelledby="help-tab">
                                <div class="terms-txt">
                                    <ul>
                                        <li>Download Club JB Mobile App from Play Store</li>
                                        <li>Register with your Country, Name & Mobile Number</li>
                                        <li>Enter your Coupon Code under Membership Section</li>
                                        <li>Enjoy your Deals !</li>
                                        <li>Others terms & conditions may apply</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="google-map" role="tabpanel" aria-labelledby="google-map-tab">
                                <div class="google-map-content pad-0 mar-0">
                                    <div id="googleMap" class="map-tab"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer-content cart-footer">
        <div class="container">
            <div class="row">
                <div class="col-2 col-md-1 pl-5 pr-0">
                    <img src="{{url('images/cart.png')}}" alt="" class="img-thumbnail mt-05">
                </div>
                <div class="col-4 col-md-5 pl-0 pr-5">
                    <div class="short-txt">
                        Actual Price
                    </div>
                    <p><span>{{$dealDetail['actual_price']}}</span> <i class="fa fa-rupee"></i>{{$dealDetail['final_price']}}</p>
                </div>
                <div class="col-6 pl-5 pr-5 text-right bg-blue">
                    <input type="hidden" id="user_token" value="{{Session::get('token')}}">
                    @if($purchased == false)
                        @php $membershipDetails = membershipDetails($user->city_id, $membershipId);@endphp
                        <a href="#" data-toggle="modal" data-target="#purchase-membership">Buy Membership</a>
                    @else
                        @if($dealDetail['payment_type'] == 2)
                            <a href="#" data-toggle="modal" data-target="#buy-summary">Buy Now</a>
                        @elseif($dealDetail['payment_type'] == 1 || $dealDetail['payment_type'] == 4 )
                            <input type="hidden" id="store_id" value="{{$dealDetail['store_id']}}">
                            <input type="hidden" id="payment_type" value="{{$dealDetail['payment_type']}}">
                            <a href="#" data-toggle="modal" data-target="#redeem-now">Redeem Now</a>
                        @elseif($dealDetail['payment_type'] == 3 )
                            <a href="#" data-toggle="modal" data-target="#call-now">Use Now</a>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </footer>
    <!-- Modal -->
    <div class="modal fade login-modal" id="buy-summary" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered buy-summary-head" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-6 pl-5">
                                <h4> Coupon Name</h4>
                            </div>
                            <div class="col-3 pl-5 pr-5">
                                <h4> Qty</h4>
                            </div>
                            <div class="col-3 pl-5 pr-5">
                                <h4> Price</h4>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-body pad-05">
                    <div class="panel-border">
                        <div class="container">
                            <div class="row">
                                <div class="col-6 pl-5">
                                    <h5>Rs 300/- Off Minimum Billing of Rs 1800/-</h5>
                                </div>
                                <div class="col-3 pl-5 pr-5">
                                    <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="quantity-left-minus btn btn-default btn-number btn-sm"
                                                data-type="minus" data-field="">
                                          <span class="fa fa-minus"></span>
                                        </button>
                                    </span>
                                        <input type="text" id="quantity" name="quantity"
                                               class="form-control input-number" value="10" min="1" max="100">
                                        <span class="input-group-btn">
                                        <button type="button" class="quantity-right-plus btn btn-primary btn-number btn-sm"
                                                data-type="plus" data-field="">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                    </span>
                                    </div>
                                </div>
                                <div class="col-3 pl-5 pr-5">
                                    <p class="mb-0"><i class="fa fa-rupee"></i> 1500/-</p>
                                    <span class="qty-left-txt">Qty Left: 1</span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="panel-border">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="jb-coins-area">
                                        <input type="checkbox">
                                        <img src="{{url('images/jb_coins.png')}}" alt="" class="img-thumbnail"> 0 JB Coins Used
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pricedetail-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 pl-5">
                                    <h4>Price Details</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-border total-amount">
                        <div class="container">
                            <div class="row">
                                <div class="col-6">
                                    <p><b>Total</b></p>
                                    <p><b>JB Coins</b></p>
                                    <p><b>Discount</b></p>
                                    <p><b>Handling Fees</b></p>
                                    <p class="fnt-2x"><b>Total Payable</b></p>
                                </div>
                                <div class="col-6 text-right">
                                    <p><i class="fa fa-rupee"></i> 1500.0</p>
                                    <p>0</p>
                                    <p>300.0</p>
                                    <p>0</p>
                                    <p class="fnt-2x"><b>1200.0</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pay-now-btn">
                        <div class="container">
                            <div class="row">
                                <div class="col-8 pl-5 pr-5">
                                    <p><b>Total Savings <i class="fa fa-rupee"></i> 300.0</b></p>
                                </div>
                                <div class="col-4 pl-5 pr-5 text-right">
                                    <button type="button" class="btn btn-primary"> Pay Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade login-modal" id="redeem-now" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Redeem Deal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" id="redeem-deal">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="text" placeholder="Enter Passcode" class="form-control" id="deal_passcode">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-default" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i> Cancel</button>
                            </div>
                            <div class="col-6">
                                <button type="button" class="btn btn-default pull-right btn-dark" onclick="redeem_membership_deal()"> <i class="fa fa-check-circle"></i> Submit</button>
                            </div>
                        </div>
                        <div class="row text-danger" id="redeem_errors">

                        </div>
                        <div class="row text-succes" id="redeem_success">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade login-modal" id="call-now" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Call Now</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <p class="clr-orange mb-0"><b><i class="fa fa-phone"></i> &nbsp;{{$dealDetail['contacts']}}</b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('shared.redeem-membership-fragment')
@endsection

@section('footerScripts')
    <script>
        var URL = '{{env('API_HOST')}}';
        $('#membership_id').val({{$membershipId}});
        $('#city_id').val({{$user->city_id}});
        $('#membership_price').val({{$membershipDetails->final_price}});
        $('#payable_membership_price').html({{$membershipDetails->final_price}});
        function myMap() {
            var mapProp = {
                center: new google.maps.LatLng(51.508742, -0.120850),
                zoom: 5,
            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        }

        function redeem_membership_deal(){
            $('#redeem_errors').html('');
            $('#redeem_success').html('');
            var token  = $('#user_token').val();
            var membership_id = $('#membership_id').val();
            var store_id = $('#store_id').val();
            var store_deal_id = $('#store_deal_id').val();
            var quantity = $('#quantity').val();
            var payment_type = $('#payment_type').val();
            var passcode = $('#deal_passcode').val();
            var form = JSON.stringify({'membership_id': membership_id, 'store_id':store_id, 'store_deal_id': store_deal_id, 'quantity': quantity, 'payment_type' :payment_type, 'passcode':passcode});
             $.ajax({
                type: 'POST',
                url: URL+"redeem-membership-deal",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                async: true,
                crossDomain: true,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+ token
                },
                data: form,
                success: function(response) {
                    console.log(response);
                    if(response.status == 406){
                         var error = '';
                         $.each(response.message, function( index, value ) {
                             error =  error + '<div class="col-12">'+value
                         });
                         $('#redeem_errors').html(error);
                    }else if(response.status == 400) {
                        var error = '<div class="col-12">' + response.message
                        '</div>';
                        $('#redeem_errors').html(error);
                    }else{
                         var message = '<div class="col-12">'+response.message
                         '</div>';
                         $('#redeem_success').html(message);
                         location.reload();
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                }
            });
        }
    </script>
@endsection
