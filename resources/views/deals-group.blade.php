@extends('shared.master')
@section('content')
    <div class="position-fixed">
        <div class="topheader-content mb-0 pad-b-03">
            <div class="row">
                <div class="col-12 pad-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 pr-5 pl-5">
                                <h6>
                                    <a href="{{ URL::previous() }}"> <i class="fa fa-arrow-left"></i></a> Deals Group
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="deals-listing deals-group">
        <div class="container">
            @foreach($dealGroups as $dealGroup)
                <div class="deal-box">
                    <a href="{{url('store_deal_bycategory/'.$membershipId.'/'.$dealGroup->store_id.'/'.getCatType($dealGroup->name))}}">
                        <div class="row">
                            <div class="col-3 pr-0">
                                <img src="{{$dealGroup->image}}" alt="" class="img-thumbnail">
                            </div>
                            <div class="col-8 pl-5 mt-1">
                                <h5 class="text-left mb-0 text-dark mt-08">{{$dealGroup->name}} <span class="pull-right text-black-50">({{$dealGroup->count}})</span></h5>

                            </div>
                            <div class="col-1 pr-5">
                                <i class="fa fa-chevron-right pull-right mt-3"></i>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>

@endsection