<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{url('css/select2.min.css')}}" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="{{url('css/style.css')}}" rel="stylesheet">
</head>

<body>
{{Form::open(array('url'=>'update_user', 'id'=>'model_city'))}}

  <div class="topheader-content">
  <div class="row">
    <div class="col-md-12">
        <h5 class="mar-0"><i class="fa fa-map-marker fa-lg"></i> Select Your Location</h5>
    </div>
  </div>
  </div>
  <div class="container">
<div class="row">

    <div class="col-md-12">
        <div class="login-form">
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                {{Form::hidden('country_id', $user->country_id, array('id'=>'model_country'))}}
                {{Form::select('state_id', $states, $user->state_id, array('class'=>'form-control select2', 'data-placeholder'=>'Select State', 'id'=>'model_states'))}}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                {{Form::select('city_id', $cities, $user->city_id, array('class'=>'form-control select2', 'data-placeholder'=>'Select City', 'id'=>'model_cities'))}}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-primary btn-block" onclick="submitCity()"> Proceed</button>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <img src="{{url('images/location-img.jpg')}}" alt="" class="card-img mt-3">
            </div>
          </div>
        </div>
    </div>


</div>
</div>
</form>
<!-- Modal -->
<div class="modal fade login-modal" id="help-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Help</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
      </div>
    </div>
  </div>
</div>
  <!-- JQuery -->
  <script type="text/javascript" src="{{url('js/jquery-3.3.1.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/select2.full.min.js')}}"></script>
 <script src="{{url('js/scripts.js')}}"></script>
  <script>
      $(function () {
          //Initialize Select2 Elements
          $('.select2').select2();
      })
      $(document).on("change","#model_states",{target : $("#model_cities")},fetchCities);
  </script>

</body>

</html>
