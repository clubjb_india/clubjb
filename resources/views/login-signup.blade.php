@extends('shared.master')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css" rel="stylesheet">

        <div class="row">
            <div class="col-md-12">
                <div class="login-logo text-center">
                    <img src="{{url('images/logo.png')}}" alt="" class="img-thumbnail">
                </div>
                <div class="membership-tabs mt-0 login-signup-content">
                    <div class="row">
                        <div class="col-12 col-md-4 mar-auto pad-0">
                            <div class="container">
                                <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login-content" role="tab"
                                           aria-controls="login-content" aria-selected="false"><i class="fa fa-user"></i> Login</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="signup-tab" data-toggle="tab" href="#signup-content" role="tab"
                                           aria-controls="signup-content" aria-selected="true"><i class="fa fa-user-plus"></i> Signup</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="membership-tab-content">
                        <div class="row">
                            <div class="col-12 col-md-4 mar-auto">
                                <div class="container">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="login-content" role="tabpanel" aria-labelledby="login-tab">
                                            {{Form::open(array('id'=>'loginForm'))}}
                                                <div class="login-form mt-3">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                                                    </div>
                                                                    <input type="text" name="phone_number" class="form-control" id="login_phone_number" placeholder="Phone Number">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fa fa-unlock-alt fa-md"></i></span>
                                                                    </div>
                                                                    <input type="password" class="form-control" name="password" id="login_password" placeholder="Password">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            <button type="button" class="btn btn-primary" onclick="loginUser()" id="logInButton"> Login</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            {{Form::close()}}
                                        </div>
                                        <div class="tab-pane fade" id="signup-content" role="tabpanel" aria-labelledby="signup-tab">
                                            {{Form::open(array('id'=>'signUpForm'))}}
                                                <div class="login-form mt-3">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h4>Location</h4>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                {{Form::select('country_id', [''=>''], '', array('class'=>'form-control select2', 'id'=>'model_country'))}}
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                {{Form::select('state_id', [''=>''], '', array('class'=>'form-control select2', 'id'=>'model_states', 'data-placeholder'=>'Select State'))}}
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                {{Form::select('city_id', [''=>''], '', array('class'=>'form-control select2', 'id'=>'model_cities', 'data-placeholder'=>'Select City'))}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h4>User Details</h4>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                                                    </div>
                                                                    {{Form::text('name', '', array('class'=>'form-control', 'placeholder'=>'First Name'))}}

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                                                    </div>
                                                                    {{Form::text('email', '', array('class'=>'form-control', 'placeholder'=>'Email'))}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <h4>Login Details</h4>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fa fa-user fa-md"></i></span>
                                                                    </div>
                                                                    {{Form::text('phone_number', '', array('class'=>'form-control', 'id'=>'register_phone_number','placeholder'=>'Mobile Number'))}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text"><i class="fa fa-unlock-alt fa-md"></i></span>
                                                                    </div>
                                                                    {{Form::input('password','password', '', array('class'=>'form-control','id'=>'register_password', 'placeholder'=>'Password'))}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="agree-txt">
                                                                <p>By Signing up you confirm that you are over 18 years of age and agree to the <a href="#" data-toggle="modal" data-target="#help-terms-txt"> Terms &amp; Conditions</a> and
                                                                    <a href="#" data-toggle="modal" data-target="#privacy-txt"> Privacy &amp; Policy</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            <button type="button" class="btn btn-primary" onClick="registerForm()" id="signUpButton"> Signup</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            {{Form::close()}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="errors" class=" text-danger">

                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal -->
    <div class="modal fade login-modal" id="help-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Help</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade login-modal" id="help-terms-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Help & T&C</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>{!! $terms !!}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade login-modal" id="privacy-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Privacy / Policy</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        {!! $privacy_policy !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade login-modal" id="otp-content" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Enter OTP</h5>
                </div>
                <div class="modal-body  otp-popup">
                    {{Form::open(array('url'=>'otp_form'))}}
                        <div class="row">
                            <div class="col-12">
                                <p class="text-black-50">A text message with OTP has been sent to: (***)***_**<span id="ph_string">93</span> </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <input type="text" class="form-control letter" autocomplete="off" name="otp_1" id="otp_1" maxlength="1" placeholder="x">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <input type="text" class="form-control letter" autocomplete="off" name="otp_2" id="otp_2" maxlength="1" placeholder="x">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <input type="text" class="form-control letter" autocomplete="off" name="otp_3"  id="otp_3" maxlength="1"  placeholder="x">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <input type="text" class="form-control letter" autocomplete="off" name="otp_4"  id="otp_4" maxlength="1" placeholder="x">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p>Didn't received <a onClick="loginUser()"> <b>Resend OTP?</b></a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <input type="hidden" id="otp_phone_number" name="phone_number" class="form-control" >
                                <button class="btn btn-primary btn-block mb-0" type="button" onClick="verifyOTPandLogin()">Verify Number</button>
                            </div>
                        </div>

                        <div class="row text-danger text-center" id="otp_errors">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footerScripts')
    <script>
            $('.letter').keypress(function ()
            {
                $(this).next().focus();
            });

        var URL = '{{env('API_HOST')}}';
        var isoCountries =  [
            @foreach($countries as $country)
            @php echo '{ id:"'.$country['id']. '",code:"'.$country['country_code'].'", text: "'.$country['country_name'].'"}'; @endphp
            @if($loop->remaining)
            ,
            @endif
            @endforeach
        ]

        function formatCountry (country) {
            if (!country.id) { return country.text; }
            var $country = $(
                '<span class="flag-icon flag-icon-'+ country.code.toLowerCase() +' flag-icon-squared"></span>' +
                '<span class="flag-text">'+ country.text+"</span>"
            );

            return $country;
        };

        $("#model_country").select2({
            placeholder: "Select a country",
            templateResult: formatCountry,
            data: isoCountries
        });
        $(document).on("change","#model_country",{target : $("#model_states")},fetchStates);
        $(document).on("change","#model_states",{target : $("#model_cities")},fetchCities);

        function registerForm(){
            $('#errors').html('');
            var form = $('#signUpForm').serialize();
            $.ajax({
                type: 'POST',
                url: URL+"register_web",
                data: form,
                success: function(response) {
                    if(response.status == 406){
                        var error = '';
                        $.each(response.message, function( index, value ) {
                            error =  error + '<div class="col-12">'+value+'</div>';
                        });
                        $('#errors').html(error);
                    }else if(response.status == 400){
                        var error =  '<div class="col-12">'+response.message+
                        '</div>';
                        $('#errors').html(error);
                    }else{
                        if(response == 'code sent'){
                            var register_phone_number = $('#register_phone_number').val();
                            var register_password = $('#register_password').val();
                            var ph_string = register_phone_number.slice(-2);
                            $('#ph_string').html(ph_string);
                            $('#otp_phone_number').val(register_phone_number);
                            $('#otp-content').modal('show');
                        }
                    }
                    console.log(response);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                }
            });
        }

        function loginUser(){
            $('#errors').html('');
            var form = $('#loginForm').serialize();
            $.ajax({
                type: 'POST',
                url: URL+"login_web",
                data: form,
                success: function(response) {
                    console.log(response);
                    if(response.status == 406){
                        var error = '';
                        $.each(response.message, function( index, value ) {
                            error =  error + '<div class="col-12">'+value+'</div>';
                        });
                        $('#errors').html(error);
                    }else if(response.status == 400){
                        var error =  '<div class="col-12">'+response.message+
                            '</div>';
                        $('#errors').html(error);
                    }else{
                        if(response == 'code sent'){
                            var login_phone_number = $('#login_phone_number').val();
                            var login_password = $('#login_password').val();
                            var ph_string = login_phone_number.slice(-2);
                            $('#ph_string').html(ph_string);
                            $('#otp_phone_number').val(login_phone_number);
                            $('#otp-content').modal('show');
                        }
                    }
                },
                error: function(error) {
                    $('#errors').html(JSON.parse(error.responseText).error);
                }
            });
        }

        function verifyOTPandLogin(){
            $('#otp_errors').html('');
            var form = $('#otp_form').serialize();
            if($('#otp_1').val() == ''){
                $('#otp_1').addClass('border-danger');
                return false;
            }
            if($('#otp_2').val() == ''){
                $('#otp_2').addClass('border-danger');
                return false;
            }
            if($('#otp_3').val() == ''){
                $('#otp_3').addClass('border-danger');
                return false;
            }
            if($('#otp_4').val() == ''){
                $('#otp_4').addClass('border-danger');
                return false;
            }
            var otp_code = $('#otp_1').val()+$('#otp_2').val()+$('#otp_3').val()+$('#otp_4').val();
            $.ajax({
                type: 'GET',
                url: window.location.origin+"/verify_otp?otp_code="+otp_code+'&phone_number='+$('#otp_phone_number').val(),
                data: form,
                success: function(response) {
                    response = JSON.parse(response);
                   // console.log(response);
                    if(response.status == 200){
                        window.location.href = window.location.origin+'/membership-price-group'
                    }else if(response.status == 406){
                        var error = '';
                        $.each(response.message, function( index, value ) {
                            error =  error + '<div class="col-12">'+value+'</div>';
                        });
                        $('#otp_errors').html(error);
                    }else if(response.status == 404){
                        $('#otp_errors').html(response.message);
                    }else{
                        $('#otp_errors').html('Something went wrong, Please try again later.');
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {

                }
            });
        }
    </script>
@endsection
