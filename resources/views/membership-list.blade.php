@extends('shared.userHeader')
@section('content')
<div class="row">
  <div class="col-12 pad-0">
    <div class="container">
      <div class="membership-tabs">
        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="purchased-membership-tab" data-toggle="tab" href="#purchased-membership" role="tab"
               aria-controls="purchased-membership" aria-selected="false"><i class="fa fa-shopping-cart"></i> Purchased Membership</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="city-membership-tab" data-toggle="tab" href="#city-membership" role="tab"
               aria-controls="city-membership" aria-selected="true"><i class="fa fa-handshake-o "></i> My City Membership</a>
          </li>

        </ul>
        <div class="membership-tab-content">
          <div class="row">
            <div class="col-12 pad-0">
              <div class="container">
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active" id="purchased-membership" role="tabpanel" aria-labelledby="purchased-membership-tab">
                    <div class="deals-listing">
                      <div class="container">
                        @foreach($purchasedMemberships->data as $purchasedMembership)
                          <div class="deal-box">
                            <div class="row">
                              <div class="col-3 pr-0 pl-5">
                                <img src="{{$purchasedMembership->image}}" alt="" class="img-thumbnail">
                              </div>
                              <div class="col-9 pl-5 mt-1">
                                <h5 class="text-left mb-0">{{$purchasedMembership->membership_name}} <span class="pull-right"><b>{{$purchasedMembership->actual_price}}</b> <i class="fa fa-rupee"></i> {{$purchasedMembership->final_price}}</span></h5>
                                <div class="text-left view-coupon"><a href="{{url('membership-categories/'.$purchasedMembership->id)}}"> View Coupons <i class="fa fa-arrow-right no-circle"></i></a> <span class="green-txt pull-right">Validity: {{$purchasedMembership->valid_days}} Days</span></div>

                              </div>
                            </div>
                          </div>
                        @endforeach

                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade " id="city-membership" role="tabpanel" aria-labelledby="city-membership-tab">
                    <div class="deals-listing">
                      <div class="container">
                        @foreach($cityMemberships->data as $membership)
                          <div class="deal-box">
                            <div class="row">
                              <div class="col-3 pr-0 pl-5">
                                <img src="{{$membership->image}}" alt="" class="img-thumbnail">
                              </div>
                              <div class="col-8 pl-5 mt-1">
                                <h5 class="text-left mb-0">{{$membership->membership_name}} <span class="pull-right"><b>{{$membership->actual_price}}</b> <i class="fa fa-rupee"></i> {{$membership->final_price}}</span></h5>
                                <div class="text-left view-coupon"><a href="{{url('membership-categories/'.$membership->id)}}"> View Coupons <i class="fa fa-arrow-right no-circle"></i></a> <span class="green-txt pull-right">Validity: {{$membership->valid_days}} Days</span></div>

                              </div>
                              <div class="col-1 pr-5">
                                <a href="" data-toggle="modal" onclick="openModal('{{$membership->id}}', '{{$user->city_id}}', '{{$membership->final_price}}')"><i class="fa fa-chevron-right pull-right mt-3"></i></a>
                              </div>
                            </div>
                          </div>
                        @endforeach

                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- SIDEBAR -->
@include('shared.redeem-membership-fragment')
</div>
@endsection
  <!-- JQuery -->

@section('footerScripts')
<script src="js/jssor.slider.min.js"></script>

<script type="text/javascript" src="js/sidebar.js"></script>
<script type="text/javascript">
    jssor_1_slider_init = function() {
        var jssor_1_options = {
            $AutoPlay: 1,
            $AutoPlaySteps: 1,
            $SlideDuration: 160,
            $SlideWidth: 980,
            $SlideSpacing: 1,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $Steps: 1
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      /*#region responsive code begin*/
        var MAX_WIDTH = 980;
        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
      /*#endregion responsive code end*/
    };
</script>
<script>
    var URL = '{{env('API_HOST')}}';
    var local_url = window.location.origin+'/';
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    });

    function openModal(membership_id, city_id, membership_price){
        $('#membership_id').val(membership_id);
        $('#city_id').val(city_id);
        $('#membership_price').val(membership_price);
        $('#payable_membership_price').html(membership_price);
        $('#purchase-membership').modal('show');

    }

    /*function buy_membership_remove(){
        var membership_id = $('#membership_id').val();
        var quantity = $('#quantity').val();
        var is_coin_used = $('#is_coin_used').val();
        $('#success').html('');
        $('#errors').html('');
        var form = JSON.stringify({'quantity': quantity, 'membership_id': membership_id, 'is_coin_used': is_coin_used});

        $.ajax({
            type: 'POST',
            url: URL+"order-membership",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            async: true,
            crossDomain: true,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ token
            },
            data: form,
            success: function(response) {
                console.log(response);
               /* if(response.status == 406){
                    var error = '';
                    $.each(response.message, function( index, value ) {
                        error =  error + '<div class="col-12">'+value
                    });
                    $('#errors').html(error);location.reload();
                }else if(response.status == 400){
                    var error =  '<div class="col-12">'+response.message
                    '</div>';
                    $('#errors').html(error);
                }else{
                    var message = '<div class="col-12">'+response.message
                    '</div>';
                    $('#success').html(message);
                    location.reload();
                }*/
            /*},
            error: function(XMLHttpRequest, textStatus, errorThrown) {

            }
        });
    }*/

</script>

@endsection
