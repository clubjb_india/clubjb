@extends('shared.master')
@section('content')
<div class="position-fixed">
<div class="topheader-content mb-0 pad-b-03">
<div class="row">
  <div class="col-12 pad-0">
    <div class="container">
      <div class="row">
        <div class="col-12 pr-5 pl-5">
          <h6>
            <a href="{{ url('membership-categories/'.$membershipId) }}"> <i class="fa fa-arrow-left"></i></a> Membership Stores
          </h6>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
  <div class="deals-listing">
    <div class="container">
      @foreach($allStores as $store)
        <div class="deal-box">
          <a href="{{url('store_dealgroups/'.$membershipId.'/'.$store->id)}}">
          <div class="row">
            <div class="col-3 pr-0">
              <img src="{{$store->image}}" alt="" class="img-thumbnail">
            </div>
            <div class="col-8 pl-5 mt-1">
              <h5 class="text-left mb-0 text-dark mt-2">{{$store->store_name}} <span class="pull-right text-black-50">({{$store->total}})</span></h5>

            </div>
            <div class="col-1 pr-5">
              <i class="fa fa-chevron-right pull-right mt-3"></i>
            </div>
          </div>
          </a>
        </div>
      @endforeach

    </div>
  </div>
</div>

</div>

@endsection
@section('footerScripts')
  <script src="{{url('js/jssor.slider.min.js')}}"></script>
  <script type="text/javascript">
    jssor_1_slider_init = function() {
      var jssor_1_options = {
        $AutoPlay: 1,
        $AutoPlaySteps: 1,
        $SlideDuration: 160,
        $SlideWidth: 980,
        $SlideSpacing: 1,
        $ArrowNavigatorOptions: {
          $Class: $JssorArrowNavigator$,
          $Steps: 1
        },
        $BulletNavigatorOptions: {
          $Class: $JssorBulletNavigator$
        }
      };

      var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      /*#region responsive code begin*/
      var MAX_WIDTH = 980;
      function ScaleSlider() {
        var containerElement = jssor_1_slider.$Elmt.parentNode;
        var containerWidth = containerElement.clientWidth;

        if (containerWidth) {

          var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

          jssor_1_slider.$ScaleWidth(expectedWidth);
        }
        else {
          window.setTimeout(ScaleSlider, 30);
        }
      }

      ScaleSlider();

      $Jssor$.$AddEvent(window, "load", ScaleSlider);
      $Jssor$.$AddEvent(window, "resize", ScaleSlider);
      $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
      /*#endregion responsive code end*/
    };
  </script>
  <script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2();
    });
  </script>


@endsection