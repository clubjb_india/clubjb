@extends('shared.userHeader')
@section('content')
    <div class="membership-deal deals-listing membership-group">
        <div class="container">
            @foreach($memPriceGroups as $memPriceGroup)
            <div class="deal-box " style="color:{{$memPriceGroup->color}}">
                <a href="{{url('membership-list/'.$memPriceGroup->priceGroup)}}">
                    <div class="row">
                        <div class="col-3 pr-0">
                            <img src="{{$memPriceGroup->image}}" alt="" class="img-thumbnail">
                            <span style="color:{{$memPriceGroup->color}}">&#x20B9;<br> {{$memPriceGroup->priceGroup}}/-</span>
                        </div>
                        <div class="col-8 pl-5 pr-10">
                            <h5 class="align-middle" style="color:{{$memPriceGroup->color}}">{{$memPriceGroup->priceGroup}}/- Memberships</h5>
                        </div>
                        <div class="col-1 pr-5">
                            <i class="fa fa-chevron-right pull-right"></i>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>


@endsection

@section('footerScripts')
    <script src="{{url('js/jssor.slider.min.js')}}"></script>

    <script type="text/javascript">
        jssor_1_slider_init = function() {
            var jssor_1_options = {
                $AutoPlay: 1,
                $AutoPlaySteps: 1,
                $SlideDuration: 160,
                $SlideWidth: 980,
                $SlideSpacing: 1,
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,
                    $Steps: 1
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/
            var MAX_WIDTH = 980;
            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>


@endsection