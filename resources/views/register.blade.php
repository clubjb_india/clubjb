@extends('shared.master')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css" rel="stylesheet">
   {{Form::open(array('url'=>'register'))}}
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="login-logo text-center">
                        <img src="images/logo.png" alt="" class="img-thumbnail">
                    </div>
                    <div class="login-form">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    {{Form::select('country_id', [''=>''], '', array('class'=>'form-control select2', 'id'=>'country_id'))}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 pr-5">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                                        </div>
                                        {{Form::text('first_name', '', array('class'=>'form-control', 'placeholder'=>'First Name'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 pl-5">
                                <div class="form-group">
                                    {{Form::text('last_name', '', array('class'=>'form-control', 'placeholder'=>'Last Name'))}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-mobile fa-lg"></i></span>
                                            </div>
                                            {{Form::text('phone_number', '', array('class'=>'form-control', 'placeholder'=>'Mobile Number'))}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-unlock-alt fa-md"></i></span>
                                            </div>
                                            {{Form::input('password','password', '', array('class'=>'form-control', 'placeholder'=>'Password'))}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-share-alt"></i></span>
                                        </div>
                                        {{Form::text('reference_id', '', array('class'=>'form-control', 'placeholder'=>'Referral Mobile (Optional)'))}}
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><a href="#" class="text-black-50" data-toggle="modal" data-target="#help-txt"><i class="fa fa-question-circle-o"></i></a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="agree-txt">
                                    <p>By Signing up you confirm that you are over 18 years of age and agree to the <a href="#" data-toggle="modal" data-target="#help-terms-txt"> Terms &amp; Conditions</a> and
                                        <a href="#" data-toggle="modal" data-target="#privacy-txt"> Privacy &amp; Policy</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary"> Register</button>
                            </div>
                        </div>
                        @if($errors->count()>0)
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade login-modal" id="help-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Help</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade login-modal" id="help-terms-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Help & T&C</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>{!! $terms !!}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade login-modal" id="privacy-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Privacy / Policy</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            {!! $privacy_policy !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('footerScripts')
    <script>
        var isoCountries =  [
                    @foreach($countries as $country)
                        @php echo '{ id:"'.$country['id']. '",code:"'.$country['country_code'].'", text: "'.$country['country_name'].'"}'; @endphp
                        @if($loop->remaining)
                            ,
                        @endif
                    @endforeach
        ]



        function formatCountry (country) {
            if (!country.id) { return country.text; }
            var $country = $(
                '<span class="flag-icon flag-icon-'+ country.code.toLowerCase() +' flag-icon-squared"></span>' +
                '<span class="flag-text">'+ country.text+"</span>"
            );

            return $country;
        };

        $("#country_id").select2({
            placeholder: "Select a country",
            templateResult: formatCountry,
            data: isoCountries
        });
    </script>
@endsection