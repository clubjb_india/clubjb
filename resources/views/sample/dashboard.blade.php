<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/select2.min.css" rel="stylesheet">
  <link href="css/sidebar.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
</head>

<div class="topheader-dropdown">
  <div class="position-fixed">
<div class="topheader-content pad-b-03">
<div class="row">
  <div class="col-12 pad-0">
    <div class="container">
      <div class="row">
        <div class="col-4 pr-0">
          <h5 class="mar-0 top-logo">           
            <img src="images/logo-top.png" alt="" class=""></h5>
        </div>
        <div class="col-4 pad-0 lh-1 text-center">
          <select class="form-control select2 mar-50">
            <option selected="">Jalandhar</option>
            <option>Ludhiana</option>
            <option>Bathinda</option>
          </select>
        </div>
        <div class="col-4  pl-0">
          <div class="coins-txt text-right">
            <img src="images/jb_coins.png" alt="">
            <p>Coins
              <span>2000</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
  <div class="dash-mar">
<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="dash-cards">
      <div class="card text-center">
        <div class="card-body dash-slider">
          <h5 class="text-center m-0">
            Offers from Club JB
          </h5>
          <div class="row">
            <div class="col-4">
              <img src="images/ccd.png" alt="" class="img-thumbnail">
            </div>
            <div class="col-4">
              <img src="images/pizza-hut.png" alt="" class="img-thumbnail">
            </div>
            <div class="col-4">
              <img src="images/carnival.png" alt="" class="img-thumbnail">
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="dash-cards">
      <div class="card pad-05">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="panel-grey">
                <div class="row">
                  <div class="col-3 pr-0">
                    <img src="images/flights-icon.png" alt="" class="img-thumbnail">
                  </div>
                  <div class="col-9 pl-0 align-middle">
                    <a href="http://travel.clubjb.com/">
                      <h5>Flights  <i class="fa fa-chevron-right pull-right"></i></h5>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel-grey">
                <div class="row">
                  <div class="col-3 pr-0">
                    <img src="images/memberships-icon.png" alt="" class="img-thumbnail">
                  </div>
                  <div class="col-9 pl-0 align-middle">
                    <a href="#">
                      <h5>Memberships  <i class="fa fa-chevron-right pull-right"></i></h5>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel-grey">
              <div class="row">
                <div class="col-3 pr-0">
                  <img src="images/horoscope-dashboard.png" alt="" class="img-thumbnail">
                </div>
                <div class="col-9 pl-0 align-middle">
                  <a href="#" onclick="myFunction()">
                  <h5>Horoscope  <i class="fa fa-chevron-right pull-right"></i></h5></a>
                  <!-- The actual snackbar -->
                  <div id="snackbar">Coming Soon..</div>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel-grey">
              <div class="row">
                <div class="col-3 pr-0">
                  <img src="images/privacy.png" alt="" class="img-thumbnail">
                </div>
                <div class="col-9 pl-0 align-middle">
                  <a href="#" data-toggle="modal" data-target="#privacy-txt">
                  <h5>Privacy/Policy  <i class="fa fa-chevron-right pull-right"></i></h5>
                  </a>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel-grey">
              <div class="row">
                <div class="col-3 pr-0">
                  <img src="images/help-icon.png" alt="" class="img-thumbnail">
                </div>
                <div class="col-9 pl-0 align-middle">
                  <a href="#" data-toggle="modal" data-target="#help-terms-txt">
                  <h5>Help & T&C  <i class="fa fa-chevron-right pull-right"></i></h5>
                  </a>
                </div>
              </div>
            </div>
            </div>
          </div>
          </div>

        </div>
      </div>
    </div>
    </div>
  <div class="btm-content">
    <p>Enjoy more exciting offers daily from various stores</p>
  </div>
  </div>
</div>
</div>
<div class="modal fade login-modal" id="privacy-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Privacy / Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
      </div>
    </div>
  </div>
</div>
<!-- SIDEBAR -->
<div data-sidebar="true" class="sidebar-trigger">
  <a class="sidebar-toggle text-black-50" href="#">
    <span class="sr-only">Sidebar toggle</span>
    <i class="fa fa-sidebar-toggle"></i>
  </a>
  <div class="sidebar-wrapper sidebar-default">
    <div class="sidebar-scroller">
      <div class="sidebar-profile">
        <div class="row">
          <div class="col-12">
            <div class="container">
          <div class="row">
          <div class="col-3 pl-5">
            <div class="profile-pic text-center pad-0">
              <i class="fa fa-user-circle-o fa-3x"></i>
            </div>
          </div>
          <div class="col-9 pl-5 pr-5">
            <h5 class="mb-0">Amandeep Singh <a href="#"><i class="fa fa-pencil pull-right"></i></a></h5>
            <p>+91 7696792992</p>
            <div class="joined-content">
              <p>Joined On: 03-05-2018</p>
            </div>
            <div class="refeeral-bg">
              <i class="fa fa-users"></i> Total Referrals : 5
            </div>

          </div>
          </div>
        </div>
          </div>
        </div>
      </div>
      <ul class="sidebar-menu">
            <li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
            <li><a href="#"><i class="fa fa-rupee"></i> My JB Coins</a></li>
            <li><a href="#"><i class="fa fa-file"></i> My Order</a></li>
            <li><a href="#"><i class="fa fa-heart-o"></i> My Favourite</a></li>
            <li><a href="#"><i class="fa fa-question-circle"></i> Help</a></li>
            <li><a href="#"><i class="fa fa-share-alt"></i> Share</a></li>
            <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>

      </ul>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade login-modal" id="help-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Help</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
      </div>
    </div>
  </div>
</div>
<div class="modal fade login-modal" id="help-terms-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Help & T&C</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
      </div>
    </div>
  </div>
</div>
<div class="modal fade login-modal" id="employmentstatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Please select your employment status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="">
        <div class="custom-control custom-radio">
          <input type="radio" class="custom-control-input" id="businessman" name="defaultExampleRadios">
          <label class="custom-control-label" for="businessman">Businessman</label>
        </div>
        <div class="custom-control custom-radio">
          <input type="radio" class="custom-control-input" id="employed" name="defaultExampleRadios" checked>
          <label class="custom-control-label" for="employed">Employed</label>
        </div>
        <div class="custom-control custom-radio">
          <input type="radio" class="custom-control-input" id="housewife" name="defaultExampleRadios">
          <label class="custom-control-label" for="housewife">Housewife</label>
        </div>
          <div class="custom-control custom-radio">
          <input type="radio" class="custom-control-input" id="student" name="defaultExampleRadios">
          <label class="custom-control-label" for="student">Student</label>
        </div>
          <div class="custom-control custom-radio">
          <input type="radio" class="custom-control-input" id="unemployed" name="defaultExampleRadios">
          <label class="custom-control-label" for="unemployed">Un-Employed</label>
        </div>
          <div class="row">
            <div class="col-md-4">
              <button type="submit" class="btn btn-primary pull-right mb-0"> Next</button>
            </div>
          </div>

        </form>
     </div>
    </div>
  </div>
</div>
  <!-- JQuery -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/select2.full.min.js"></script>
<script src="js/hammer.min.js"></script>
  <script type="text/javascript" src="js/sidebar.js"></script>

  <script>
      $(function () {
          //Initialize Select2 Elements
          $('.select2').select2();
      })
      $.get('https://raw.githubusercontent.com/FortAwesome/Font-Awesome/fa-4/src/icons.yml', function(data) {

          var parsedYaml = jsyaml.load(data);
          var options = new Array();
          $.each(parsedYaml.icons, function(index, icon){
              options.push({
                  id: icon.id,
                  text: '<i class="fa fa-fw fa-' + icon.id + '"></i> ' + icon.id
              });
          });
      });

  </script>
<script>
    function myFunction() {
        var x = document.getElementById("snackbar");
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    $(window).load(function(){
        $('#employmentstatus').modal('show');
    });
</script>

</body>

</html>
