<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/select2.min.css" rel="stylesheet">
  <link href="css/sidebar.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
</head>
<body class="clr-grey">
<div class="cart-btm">
<div class="deals-cart">
  <div class="row">
    <div class="col-12">
      <div class="main-img">
        <img src="images/300off.jpg" alt="" class="img-thumbnail text-center">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="container">
        <div class="row">
          <div class="col-9 pl-5 pr-5">
            <div class="dealname-txt">
              <h5 class="deal-name">Rs 300/- Off Minimum Billing of Rs 1800/-</h5>
            </div>
          </div>
          <div class="col-3 pl-5 pr-5">
            <div class="icon-circle">
              <a href="#"><i class="fa fa-share-alt clr-orange"></i></a>
              <a href="#"><i class="fa fa-heart-o"></i></a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6 pl-5 pr-5">
            <div class="storename-txt">
              <span class="text-black-50 mb-0">Valid For 365 Days</span>
              <a href="#">Carnival Cinemas <i class="fa fa-arrow-right"></i></a>
            </div>
          </div>
          <div class="col-6 pl-5 pr-5">
            <div class="price-txt pull-right">
              <p><b class="short-txt">Actual Deal Price{{-- <a href="#"><i
                            class="fa fa-question-circle pull-right"></i></a>--}}</b> <span>1800/-</span> <i
                        class="fa fa-rupee"></i> 1500/-</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-5 pl-5 pr-5">
            <div class="features-txt">
              <img src="images/gold-icon.png" alt="" class="img-thumbnail">
              <span class="jb-ratings">
              <img src="images/logo-sm.png" alt="" class="img-thumbnail">
              <b>5.0</b>
            </span>
            </div>
          </div>
          <div class="col-7 pl-5 pr-5">
            <div class="buynow-txt pull-right mt-1">
              <span>Savings</span> <i class="fa fa-rupee"></i>100/-
            </div>
          </div>
        </div>
        <hr/>
        <div class="row">
          <div class="col-6 pl-5 pr-5">
            <div class="bought-txt">
              <p> 500+ Bought
                <a href="#">Qty Left:1</a>
              </p>
            </div>
          </div>
          <div class="col-6 pl-5 pr-5">
            <div class="ratings-star">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
            </div>
          </div>
        </div>
        <div class="hr-border0">
          <hr/>
        </div>
        <div class="row">
          <div class="col-12 pad-0">
            <div class="deals-tabs">
              <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                <li class="nav-item active">
                  <a class="nav-link" id="address-tab" data-toggle="tab" href="#address" role="tab"
                     aria-controls="address" aria-selected="false">Address</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="terms-tab" data-toggle="tab" href="#terms" role="tab"
                     aria-controls="terms" aria-selected="false">T&C</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="menu-pics-tab" data-toggle="tab" href="#menu-pics"
                     role="tab" aria-controls="menu-pics" aria-selected="false">Menus & Pics</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="help-tab" data-toggle="tab" href="#help" role="tab"
                     aria-controls="help" aria-selected="false">Help</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="google-map-tab" data-toggle="tab" href="#google-map"
                     role="tab" aria-controls="googl-map" aria-selected="false">Google Map</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="deals-tab-content">
  <div class="row">
    <div class="col-12 pr-5 pl-5">
      <div class="container">
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="address" role="tabpanel" aria-labelledby="address-tab">
            <div class="address-txt">
              <h5><i class="fa fa-home"></i> Address</h5>
              <p>3rd Floor, Over Oriental Bank of Commerce, Jalandhar, 144001</p>
              <h5><i class="fa fa-phone"></i> Mobile</h5>
              <p>81960 81960</p>
              <h5><i class="fa fa-envelope-o"></i> Email</h5>
              <p>info@clubjb.com</p>
            </div>
          </div>
          <div class="tab-pane fade" id="terms" role="tabpanel" aria-labelledby="terms-tab">
            <div class="terms-txt">
              <ul>
                <li>Valid on All Class</li>
                <li>Only one coupon can be used in one day</li>
                <li>Available through Club JB Mobile App/Website only</li>
                <li>Please Register your booklet on our App to use the Coupons</li>
                <li>No refund will be given in case the total used value is less
                  than the coupon amount used.
                </li>
              </ul>
            </div>
          </div>
          <div class="tab-pane fade" id="menu-pics" role="tabpanel" aria-labelledby="menu-pics-tab">
            <div class="terms-txt">
             <p class="text-center text-black-50 mt-4">No data found!</p>
            </div>
          </div>
          <div class="tab-pane fade" id="help" role="tabpanel" aria-labelledby="help-tab">
            <div class="terms-txt">
              <ul>
                <li>Download Club JB Mobile App from Play Store</li>
                <li>Register with your Country, Name & Mobile Number</li>
                <li>Enter your Coupon Code under Membership Section</li>
                <li>Enjoy your Deals !</li>
                <li>Others terms & conditions may apply</li>
              </ul>
            </div>
          </div>
          <div class="tab-pane fade" id="google-map" role="tabpanel" aria-labelledby="google-map-tab">
            <div class="google-map-content pad-0 mar-0">
              <div id="googleMap" class="map-tab"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade login-modal" id="buy-summary" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered buy-summary-head" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="container">
          <div class="row">
            <div class="col-6 pl-5">
              <h4> Coupon Name</h4>
            </div>
            <div class="col-3 pl-5 pr-5">
              <h4> Qty</h4>
            </div>
            <div class="col-3 pl-5 pr-5">
              <h4> Price</h4>
            </div>

          </div>
        </div>
      </div>
      <div class="modal-body pad-05">
        <div class="panel-border">
          <div class="container">
            <div class="row">
              <div class="col-6 pl-5">
                <h5>Rs 300/- Off Minimum Billing of Rs 1800/-</h5>
              </div>
              <div class="col-3 pl-5 pr-5">
                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="quantity-left-minus btn btn-default btn-number btn-sm"
                                                data-type="minus" data-field="">
                                          <span class="fa fa-minus"></span>
                                        </button>
                                    </span>
                  <input type="text" id="quantity" name="quantity"
                         class="form-control input-number" value="10" min="1" max="100">
                  <span class="input-group-btn">
                                        <button type="button" class="quantity-right-plus btn btn-primary btn-number btn-sm"
                                                data-type="plus" data-field="">
                                            <span class="fa fa-plus"></span>
                                        </button>
                                    </span>
                </div>
              </div>
              <div class="col-3 pl-5 pr-5">
                <p class="mb-0"><i class="fa fa-rupee"></i> 1500/-</p>
                <span class="qty-left-txt">Qty Left: 1</span>
              </div>

            </div>
          </div>
        </div>
        <div class="panel-border">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="jb-coins-area">
                  <input type="checkbox">
                  <img src="images/jb_coins.png" alt="" class="img-thumbnail"> 0 JB Coins Used
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="pricedetail-content">
          <div class="container">
            <div class="row">
              <div class="col-12 pl-5">
                <h4>Price Details</h4>
              </div>
            </div>
          </div>
        </div>
        <div class="panel-border total-amount">
          <div class="container">
            <div class="row">
              <div class="col-6">
                <p><b>Total</b></p>
                <p><b>JB Coins</b></p>
                <p><b>Discount</b></p>
                <p><b>Handling Fees</b></p>
                <p class="fnt-2x"><b>Total Payable</b></p>
              </div>
              <div class="col-6 text-right">
                <p><i class="fa fa-rupee"></i> 1500.0</p>
                <p>0</p>
                <p>300.0</p>
                <p>0</p>
                <p class="fnt-2x"><b>1200.0</b></p>
              </div>
            </div>
          </div>
        </div>
        <div class="pay-now-btn">
          <div class="container">
            <div class="row">
              <div class="col-8 pl-5 pr-5">
                <p><b>Total Savings <i class="fa fa-rupee"></i> 300.0</b></p>
              </div>
              <div class="col-4 pl-5 pr-5 text-right">
                <button type="button" class="btn btn-primary"> Pay Now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer class="footer-content cart-footer">
  <div class="container">
    <div class="row">
      <div class="col-2 col-md-1 pl-5 pr-0">
        <img src="images/cart.png" alt="" class="img-thumbnail mt-05">
      </div>
      <div class="col-4 col-md-5 pl-0 pr-5">
        <div class="short-txt">
          Actual Price
        </div>
        <p><span>300</span> <i class="fa fa-rupee"></i>250</p>
      </div>
      <div class="col-6 pl-5 pr-5 text-right bg-blue">
        <a href="#" data-toggle="modal" data-target="#buy-summary">Buy Now</a>
      </div>
    </div>
  </div>
</footer>
<!-- JQuery -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/select2.full.min.js"></script>
<script src="js/jssor.slider.min.js"></script>
<script src="js/hammer.min.js"></script>
<script type="text/javascript" src="js/sidebar.js"></script>
<script type="text/javascript">
    jssor_1_slider_init = function () {
        var jssor_1_options = {
            $AutoPlay: 1,
            $AutoPlaySteps: 1,
            $SlideDuration: 160,
            $SlideWidth: 980,
            $SlideSpacing: 1,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $Steps: 1
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      /*#region responsive code begin*/
        var MAX_WIDTH = 980;

        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
      /*#endregion responsive code end*/
    };
</script>
<script>
    $('.nav-tabs a').on('click', function () {
        $('.nav-tabs').find('li.active').removeClass('active');
        $(this).parent('li').addClass('active');
    });
</script>
<script>
    function myMap() {
        var mapProp = {
            center: new google.maps.LatLng(51.508742, -0.120850),
            zoom: 5,
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY&callback=myMap"></script>
<script>
    $(document).ready(function(){

        var quantitiy=0;
        $('.quantity-right-plus').click(function(e){

            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            $('#quantity').val(quantity + 1);


            // Increment

        });

        $('.quantity-left-minus').click(function(e){
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            var quantity = parseInt($('#quantity').val());

            // If is not undefined

            // Increment
            if(quantity>0){
                $('#quantity').val(quantity - 1);
            }
        });

    });
</script>

<script>
    $('.select2').select2();
    jssor_1_slider_init()
</script>
  <script>
      $(".deal-name").each(function(i){
          len=$(this).text().length;
          if(len>60)
          {
              $(this).text($(this).text().substr(0,60)+'...');
          }
      });
  </script>
</body>

</html>
