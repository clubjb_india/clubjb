<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/select2.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
</head>

<body>
<form action="">
<div class="topheader-content">
<div class="row">
  <div class="col-md-12">
      <h5 class="mar-0"><i class="fa fa-map-marker fa-lg"></i> Select Your Location</h5>
  </div>
</div>
</div>
<div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="login-form">
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <select class="form-control select2">
                <option selected="">Select State</option>
                <option>Punjab</option>
                <option>Uttar Pardesh</option>
                <option>Haryana</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <select class="form-control select2">
                <option selected="">Select City</option>
                <option>Jalandhar</option>
                <option>Ludhiana</option>
                <option>Bathinda</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <button type="button" class="btn btn-primary btn-block"> Proceed</button>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <img src="images/location-img.jpg" alt="" class="card-img mt-3">
          </div>
        </div>
  </div>
</div>
</div>
</div>
</form>
<!-- Modal -->
<div class="modal fade login-modal" id="help-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Help</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
      </div>
    </div>
  </div>
</div>
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/select2.full.min.js"></script>
  <script>
      $(function () {
          //Initialize Select2 Elements
          $('.select2').select2();
      })
  </script>

</body>

</html>
