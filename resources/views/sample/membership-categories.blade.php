<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/select2.min.css" rel="stylesheet">
  <link href="css/sidebar.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
</head>
<body class="clr-grey">
<div class="position-fixed">
<div class="topheader-content mb-0 pad-b-03">
<div class="row">
  <div class="col-12 pad-0">
    <div class="container">
      <div class="row">
        <div class="col-12 pr-5 pl-5">
          <h6>
            <a href="#"> <i class="fa fa-arrow-left"></i></a> Store Categories
          </h6>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
  <div class="deals-listing membership-categories">
    <div class="container">
      <div class="deal-box">
        <a href="#">
        <div class="row">
          <div class="col-3 pr-0">
            <img src="images/movies.png" alt="" class="img-thumbnail">
          </div>
          <div class="col-8 pl-5 mt-1">
            <h5 class="text-left mb-0 text-dark mt-08">Movies & Entertainment <span class="pull-right text-black-50">(4)</span></h5>

          </div>
          <div class="col-1 pr-5">
            <i class="fa fa-chevron-right pull-right mt-3"></i>
          </div>
        </div>
        </a>
      </div>
      <div class="deal-box">
        <a href="#">
        <div class="row">
          <div class="col-3 pr-0">
            <img src="images/fitness.png" alt="" class="img-thumbnail">
          </div>
          <div class="col-8 pl-5 mt-1">
            <h5 class="text-left mb-0 text-dark mt-08">Body & Fitness <span class="pull-right text-black-50">(8)</span></h5>

          </div>
          <div class="col-1 pr-5">
            <i class="fa fa-chevron-right pull-right mt-3"></i>
          </div>
        </div>
        </a>
      </div>

    </div>
  </div>


  <!-- JQuery -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/select2.full.min.js"></script>
<script src="js/jssor.slider.min.js"></script>
<script src="js/hammer.min.js"></script>
<script type="text/javascript" src="js/sidebar.js"></script>
<script type="text/javascript">
    jssor_1_slider_init = function() {
        var jssor_1_options = {
            $AutoPlay: 1,
            $AutoPlaySteps: 1,
            $SlideDuration: 160,
            $SlideWidth: 980,
            $SlideSpacing: 1,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $Steps: 1
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      /*#region responsive code begin*/
        var MAX_WIDTH = 980;
        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
      /*#endregion responsive code end*/
    };
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    });
</script>


</body>

</html>
