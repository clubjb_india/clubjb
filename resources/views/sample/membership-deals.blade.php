<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/select2.min.css" rel="stylesheet">
  <link href="css/sidebar.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
</head>
<body class="clr-grey">
<div class="position-fixed">
<div class="topheader-content mb-0 pad-b-03">
<div class="row">
  <div class="col-12 pad-0">
    <div class="container">
      <div class="row">
        <div class="col-12 pr-5 pl-5">
          <h6>
            <a href="#"> <i class="fa fa-arrow-left"></i></a> Exclusive Deals (2)
          </h6>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="deals-listview">
  <div class="container pl-5 pr-5">
    <div class="deal-list-box">
      <a href="#" data-toggle="modal" data-target="#call-now">
      <div class="row">
        <div class="col-4 pr-5">
          <img src="images/300off.jpg" alt="" class="img-thumbnail">
        </div>
        <div class="col-8 pl-5">
          <div class="row">
            <div class="col-12">
              <p class="deal-name">Rs 300/- off Minimum Billing of Rs 1800/-</p>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="price-txt">
                <p><b class="short-txt">Actual Deal Price</b> <span>1800/-</span> <i class="fa fa-rupee"></i>1500/-</p>
              </div>
            </div>
            <div class="col-6">
              <div class="buynow-txt buy-btn">
              <button type="button" class="btn btn-primary"> Buy Now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr/>
      <div class="row">
        <div class="col-7 pr-5">
          <div class="terms-bg">
            <span class="qty-txt">
            QTY:- 1
            </span>
            <a href="#">T&amp;C</a>
            <a href="#" data-toggle="modal" data-target="#help-txt"><i class="fa fa-question-circle text-black-50"></i></a>
            <a href="#">Deal Type</a>
          </div>
        </div>
        <div class="col-5 pl-5">
          <div class="buynow-txt">
            <span>Savings upto</span> <i class="fa fa-rupee"></i>300/-
          </div>
        </div>
      </div>
      </a>
    </div>
    <div class="deal-list-box">
      <a href="#">
      <div class="row">
        <div class="col-4 pr-5">
          <img src="images/100off.jpg" alt="" class="img-thumbnail">
        </div>
        <div class="col-8 pl-5">
          <div class="row">
            <div class="col-12">
              <p class="deal-name">Rs 100/- off Minimum Billing of Rs 500/-</p>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="price-txt">
                <p><b class="short-txt">Actual Deal Price</b> <span>500/-</span> <i class="fa fa-rupee"></i>400/-</p>
              </div>
            </div>
            <div class="col-6">
              <div class="buynow-txt buy-btn">
              <button type="button" class="btn btn-primary"> Buy Now</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr/>
      <div class="row">
        <div class="col-7 pr-5">
          <div class="terms-bg">
            <span class="qty-txt">
            QTY:- 1
            </span>
            <a href="#">T&amp;C</a>
            <a href="#" data-toggle="modal" data-target="#help-txt"><i class="fa fa-question-circle text-black-50"></i></a>
            <a href="#">Deal Type</a>
          </div>
        </div>
        <div class="col-5 pl-5">
          <div class="buynow-txt">
            <span>Savings upto</span> <i class="fa fa-rupee"></i>100/-
          </div>
        </div>
      </div>
      </a>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade login-modal" id="help-txt" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Help</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade login-modal" id="redeem-now" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Redeem Deal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="">
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                <input type="text" placeholder="Enter Passcode" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <button type="button" class="btn btn-default" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i> Cancel</button>
            </div>
            <div class="col-6">
              <button type="button" class="btn btn-default pull-right btn-dark"> <i class="fa fa-check-circle"></i> Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade login-modal" id="call-now" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Call Now</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
            <div class="col-12">
              <p class="clr-orange mb-0"><b>+91 8196081960, 81960 81962</b></p>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
  <!-- JQuery -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/select2.full.min.js"></script>
<script src="js/jssor.slider.min.js"></script>
<script src="js/hammer.min.js"></script>
<script type="text/javascript" src="js/sidebar.js"></script>
<script type="text/javascript">
    jssor_1_slider_init = function() {
        var jssor_1_options = {
            $AutoPlay: 1,
            $AutoPlaySteps: 1,
            $SlideDuration: 160,
            $SlideWidth: 980,
            $SlideSpacing: 1,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $Steps: 1
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      /*#region responsive code begin*/
        var MAX_WIDTH = 980;
        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
      /*#endregion responsive code end*/
    };
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
        $(".deal-name").each(function(i){
            len=$(this).text().length;
            if(len>60)
            {
                $(this).text($(this).text().substr(0,60)+'...');
            }
        });
    });
</script>


</body>

</html>
