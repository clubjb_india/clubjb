<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/select2.min.css" rel="stylesheet">
  <link href="css/sidebar.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
</head>
<body class="clr-grey">
<div class="position-fixed">
  <div class="topheader-content mb-0 pad-b-03">
    <div class="row">
      <div class="col-12 pad-0">
        <div class="container">
          <div class="row">
            <div class="col-4 pr-0">
              <h5 class="mar-0 top-logo">
                <img src="images/logo-top.png" alt="" class=""></h5>
            </div>
            <div class="col-4 pad-0 text-center location-select">
              <a href="#" data-toggle="modal" data-target="#location-change">Jalandhar <i class="fa fa-sort-down"></i></a>
            </div>
            <div class="col-4 pl-0">
              <div class="coins-txt text-right">
                <img src="images/jb_coins.png" alt="">
                <p>Coins
                  <span>2000</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12 pad-0">
    <div class="container">
      <div class="membership-tabs">
        <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="purchased-membership-tab" data-toggle="tab" href="#purchased-membership" role="tab"
               aria-controls="purchased-membership" aria-selected="false"><i class="fa fa-shopping-cart"></i> Purchased Membership</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="city-membership-tab" data-toggle="tab" href="#city-membership" role="tab"
               aria-controls="city-membership" aria-selected="true"><i class="fa fa-handshake-o "></i> My City Membership</a>
          </li>

        </ul>
        <div class="membership-tab-content">
          <div class="row">
            <div class="col-12 pad-0">
              <div class="container">
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active" id="purchased-membership" role="tabpanel" aria-labelledby="purchased-membership-tab">
                    <div class="deals-listing">
                      <div class="container">
                        <div class="deal-box">
                          <div class="row">
                            <div class="col-3 pr-0 pl-5">
                              <img src="images/selectplus.jpg" alt="" class="img-thumbnail">
                            </div>
                            <div class="col-9 pl-5 mt-1">
                              <h5 class="text-left mb-0">JB Select Plus <span class="pull-right"><b>500</b> <i class="fa fa-rupee"></i> 300</span></h5>
                              <div class="text-left view-coupon"><a href="#"> View Coupons <i class="fa fa-arrow-right no-circle"></i></a> <span class="green-txt pull-right">Validity: 90 Days</span></div>

                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade " id="city-membership" role="tabpanel" aria-labelledby="city-membership-tab">
                    <div class="deals-listing">
                      <div class="container">
                        <div class="deal-box">
                          <div class="row">
                            <div class="col-3 pr-0 pl-5">
                              <img src="images/selectplus.jpg" alt="" class="img-thumbnail">
                            </div>
                            <div class="col-8 pl-5 mt-1">
                              <h5 class="text-left mb-0">JB Select Plus <span class="pull-right"><b>500</b> <i class="fa fa-rupee"></i> 300</span></h5>
                              <div class="text-left view-coupon"><a href="#"> View Coupons <i class="fa fa-arrow-right no-circle"></i></a> <span class="green-txt pull-right">Validity: 90 Days</span></div>

                            </div>
                            <div class="col-1 pr-5">
                              <a href="#" data-toggle="modal" data-target="#purchase-membership"><i class="fa fa-chevron-right pull-right mt-3"></i></a>
                            </div>
                          </div>
                        </div>
                        <div class="deal-box">
                          <div class="row">
                            <div class="col-3 pr-0 pl-5">
                              <img src="images/selectplus.jpg" alt="" class="img-thumbnail">
                            </div>
                            <div class="col-8 pl-5 mt-1">
                              <h5 class="text-left mb-0">JB Mniature <span class="pull-right"><b>500</b> <i class="fa fa-rupee"></i> 300</span></h5>
                              <div class="text-left view-coupon"><a href="#"> View Coupons <i class="fa fa-arrow-right no-circle"></i></a> <span class="green-txt pull-right">Validity: 90 Days</span></div>

                            </div>
                            <div class="col-1 pr-5">
                              <a href="#" data-toggle="modal" data-target="#purchase-membership"><i class="fa fa-chevron-right pull-right mt-3"></i></a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- SIDEBAR -->
<div data-sidebar="true" class="sidebar-trigger">
  <a class="sidebar-toggle text-black-50" href="#">
    <span class="sr-only">Sidebar toggle</span>
    <i class="fa fa-sidebar-toggle"></i>
  </a>
  <div class="sidebar-wrapper sidebar-default">
    <div class="sidebar-scroller">
      <div class="sidebar-profile">
        <div class="row">
          <div class="col-12">
            <div class="container">
          <div class="row">
          <div class="col-3 pl-5">
            <div class="profile-pic text-center pad-0">
              <i class="fa fa-user-circle-o fa-3x"></i>
            </div>
          </div>
          <div class="col-9 pl-5 pr-5">
            <h5 class="mb-0">Amandeep Singh <a href="#"><i class="fa fa-pencil pull-right"></i></a></h5>
            <p>+91 7696792992</p>
            <div class="refeeral-bg">
              <i class="fa fa-users"></i> Total Referrals : 5
            </div>

          </div>
          </div>
        </div>
          </div>
        </div>
      </div>
      <ul class="sidebar-menu">
            <li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
            <li><a href="#"><i class="fa fa-rupee"></i> My JB Coins</a></li>
            <li><a href="#"><i class="fa fa-file"></i> My Order</a></li>
            <li><a href="#"><i class="fa fa-heart-o"></i> My Favourite</a></li>
            <li><a href="#"><i class="fa fa-question-circle"></i> Help</a></li>
            <li><a href="#"><i class="fa fa-share-alt"></i> Share</a></li>
            <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
      </ul>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade login-modal" id="location-change" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle"><i class="fa fa-map-marker fa-lg"></i> Select Your Location</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="login-form">
          <form action="">
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <select class="form-control select2">
                    <option selected="">Select State</option>
                    <option>Punjab</option>
                    <option>Uttar Pardesh</option>
                    <option>Haryana</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="form-group">
                  <select class="form-control select2">
                    <option selected="">Select City</option>
                    <option>Jalandhar</option>
                    <option>Ludhiana</option>
                    <option>Bathinda</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-block mb-0 mt-0"> Proceed</button>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade login-modal" id="purchase-membership" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Redeem Membership</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="">
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                <input type="text" placeholder="Enter Membership Code" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <button type="button" class="btn btn-default" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i> Cancel</button>
            </div>
            <div class="col-6">
              <button type="button" class="btn btn-default pull-right btn-dark"> <i class="fa fa-check-circle"></i> Submit</button>
            </div>
          </div>
          <hr/>
          <div class="row">
            <div class="col-12">
              <div class="text-center">
                <button type="button" class="btn btn-primary mb-0"> Pay <i class="fa fa-rupee"></i> 2999.0</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

</div>

  <!-- JQuery -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/select2.full.min.js"></script>
<script src="js/jssor.slider.min.js"></script>
<script src="js/hammer.min.js"></script>
<script type="text/javascript" src="js/sidebar.js"></script>
<script type="text/javascript">
    jssor_1_slider_init = function() {
        var jssor_1_options = {
            $AutoPlay: 1,
            $AutoPlaySteps: 1,
            $SlideDuration: 160,
            $SlideWidth: 980,
            $SlideSpacing: 1,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $Steps: 1
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      /*#region responsive code begin*/
        var MAX_WIDTH = 980;
        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
      /*#endregion responsive code end*/
    };
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2();
    });
</script>


</body>

</html>
