<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/select2.min.css" rel="stylesheet">
  <link href="css/sidebar.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
</head>
<body class="clr-grey">
<div class="position-fixed">
<div class="topheader-content mb-0 pad-b-03">
<div class="row">
  <div class="col-12 pad-0">
    <div class="container">
      <div class="row">
        <div class="col-4 pr-0">
          <h5 class="mar-0 top-logo">           
            <img src="images/logo-top.png" alt="" class=""></h5>
        </div>
        <div class="col-4 pad-0 text-center location-select">
          <a href="#" data-toggle="modal" data-target="#location-change">Jalandhar <i class="fa fa-sort-down"></i></a>
        </div>
        <div class="col-4 pl-0">
          <div class="coins-txt text-right">
            <img src="images/jb_coins.png" alt="">
            <p>Coins
              <span>2000</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="membership-deal deals-listing membership-group">
  <div class="container">
    <div class="deal-box color-blue">
      <a href="#">
    <div class="row">
      <div class="col-3 pr-0">
        <img src="images/pricegroup_09A7EB.png" alt="" class="img-thumbnail">
        <span>&#x20B9;<br> 1199/-</span>
      </div>
      <div class="col-8 pl-5 pr-10">
        <h5 class="align-middle">1199/- Memberships</h5>
      </div>
      <div class="col-1 pr-5">
        <i class="fa fa-chevron-right pull-right"></i>
      </div>
    </div>
      </a>
  </div>
    <div class="deal-box color-green">
      <a href="#">
      <div class="row">
        <div class="col-3 pr-0">
          <img src="images/pricegroup_009846.png" alt="" class="img-thumbnail">
          <span>&#x20B9;<br> 999/-</span>
        </div>
        <div class="col-8 pl-5 pr-10">
          <h5 class="align-middle">999/- Memberships</h5>
        </div>
        <div class="col-1 pr-5">
          <i class="fa fa-chevron-right pull-right"></i>
        </div>
      </div>
      </a>
    </div>
    <div class="deal-box color-maroon">
      <a href="#">
      <div class="row">
        <div class="col-3 pr-0">
          <img src="images/pricegroup_C6254A.png" alt="" class="img-thumbnail">
          <span>&#x20B9;<br> 799/-</span>
        </div>
        <div class="col-8 pl-5 pr-10">
          <h5 class="align-middle">799/- Memberships</h5>
        </div>
        <div class="col-1 pr-5">
          <i class="fa fa-chevron-right pull-right"></i>
        </div>
      </div>
      </a>
    </div>
    <div class="deal-box color-orange">
      <a href="#">
        <div class="row">
          <div class="col-3 pr-0">
            <img src="images/pricegroup_E05124.png" alt="" class="img-thumbnail">
            <span>&#x20B9;<br> 399/-</span>
          </div>
          <div class="col-8 pl-5 pr-10">
            <h5 class="align-middle">399/- Memberships</h5>
          </div>
          <div class="col-1 pr-5">
            <i class="fa fa-chevron-right pull-right"></i>
          </div>
        </div>
      </a>
    </div>
    <div class="deal-box color-magenta">
      <a href="#">
      <div class="row">
        <div class="col-3 pr-0">
          <img src="images/pricegroup_E51A4B.png" alt="" class="img-thumbnail">
          <span>&#x20B9;<br> 299/-</span>
        </div>
        <div class="col-8 pl-5 pr-10">
          <h5 class="align-middle">299/- Memberships</h5>
        </div>
        <div class="col-1 pr-5">
          <i class="fa fa-chevron-right pull-right"></i>
        </div>
      </div>
      </a>
    </div>
    <div class="deal-box color-yellow">
      <a href="#">
      <div class="row">
        <div class="col-3 pr-0">
          <img src="images/pricegroup_F8AE2A.png" alt="" class="img-thumbnail">
          <span>&#x20B9;<br> 199/-</span>
        </div>
        <div class="col-8 pl-5 pr-10">
          <h5 class="align-middle">199/- Memberships</h5>
        </div>
        <div class="col-1 pr-5">
          <i class="fa fa-chevron-right pull-right"></i>
        </div>
      </div>
      </a>
    </div>


  </div>
</div>

<!-- SIDEBAR -->
<div data-sidebar="true" class="sidebar-trigger">
  <a class="sidebar-toggle text-black-50" href="#">
    <span class="sr-only">Sidebar toggle</span>
    <i class="fa fa-sidebar-toggle"></i>
  </a>
  <div class="sidebar-wrapper sidebar-default">
    <div class="sidebar-scroller">
      <div class="sidebar-profile">
        <div class="row">
          <div class="col-12">
            <div class="container">
          <div class="row">
          <div class="col-3 pl-5">
            <div class="profile-pic text-center pad-0">
              <i class="fa fa-user-circle-o fa-3x"></i>
            </div>
          </div>
          <div class="col-9 pl-5 pr-5">
            <h5 class="mb-0">Amandeep Singh <a href="#"><i class="fa fa-pencil pull-right"></i></a></h5>
            <p>+91 7696792992</p>
            <div class="refeeral-bg">
              <i class="fa fa-users"></i> Total Referrals : 5
            </div>

          </div>
          </div>
        </div>
          </div>
        </div>
      </div>
      <ul class="sidebar-menu">
            <li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
            <li><a href="#"><i class="fa fa-rupee"></i> My JB Coins</a></li>
            <li><a href="#"><i class="fa fa-file"></i> My Order</a></li>
            <li><a href="#"><i class="fa fa-heart-o"></i> My Favourite</a></li>
            <li><a href="#"><i class="fa fa-question-circle"></i> Help</a></li>
            <li><a href="#"><i class="fa fa-share-alt"></i> Share</a></li>
            <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
      </ul>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade login-modal" id="location-change" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle"><i class="fa fa-map-marker fa-lg"></i> Select Your Location</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="login-form">
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                <select class="form-control select2">
                  <option selected="">Select State</option>
                  <option>Punjab</option>
                  <option>Uttar Pardesh</option>
                  <option>Haryana</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                <select class="form-control select2">
                  <option selected="">Select City</option>
                  <option>Jalandhar</option>
                  <option>Ludhiana</option>
                  <option>Bathinda</option>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary btn-block mb-0 mt-0"> Proceed</button>
            </div>
          </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>

  <!-- JQuery -->
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/select2.full.min.js"></script>
<script src="js/jssor.slider.min.js"></script>
<script src="js/hammer.min.js"></script>
<script type="text/javascript" src="js/sidebar.js"></script>
<script type="text/javascript">
    jssor_1_slider_init = function() {
        var jssor_1_options = {
            $AutoPlay: 1,
            $AutoPlaySteps: 1,
            $SlideDuration: 160,
            $SlideWidth: 980,
            $SlideSpacing: 1,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $Steps: 1
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      /*#region responsive code begin*/
        var MAX_WIDTH = 980;
        function ScaleSlider() {
            var containerElement = jssor_1_slider.$Elmt.parentNode;
            var containerWidth = containerElement.clientWidth;

            if (containerWidth) {

                var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                jssor_1_slider.$ScaleWidth(expectedWidth);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();

        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
      /*#endregion responsive code end*/
    };
</script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2({
            dropdownParent: $('#location-change')
        })
    });
    $(window).load(function(){
        $('#location-change').modal('show');
    });
</script>


</body>

</html>
