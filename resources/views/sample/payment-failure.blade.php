<!DOCTYPE HTML>
<html>
   <head>
      <title>Club JB</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="" />
      <!-- Bootstrap Core CSS -->
      <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
      <!-- Custom CSS -->
      <link href="css/style.css" rel='stylesheet' type='text/css' />
      <!-- Font Awesome CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body class="clr-grey">
      <div class="container">
         <div class="row">
         <div class="col-md-12 no-padding">
            <div class="logo text-center mt-4"><img src="images/logo.png" class="img-responsive" /></div>
            <div class="successfull-inner color-red">
               <i class="fa fa-times"></i>
               <h3>Transaction Failure</h3>
               <p>We have sent you an email with all the details of your order & remember you can track your order using this app!</p>              
            </div>
            <div class="bottom-btns">
               <button type="button" class="btn btn-primary pull-left">Ok</button>
               <button type="button" class="btn btn-default pull-right">Cancel</button>
            </div>
         </div>
      </div>
      </div>
   </body>
</html>