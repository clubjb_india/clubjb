<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('css/select2.min.css')}}" rel="stylesheet">
    <link href="{{url('css/sidebar.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{url('css/style.css')}}" rel="stylesheet">
</head>
<body>
@yield('content')

<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/select2.full.min.js')}}"></script>
<script src="{{url('js/hammer.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/sidebar.js')}}"></script>
<script type="text/javascript" src="{{url('js/scripts.js')}}"></script>

@yield('footerScripts')

</body>

</html>
