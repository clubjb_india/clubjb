<div class="modal fade login-modal" id="purchase-membership" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Redeem Membership</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <input type="text" placeholder="Enter Membership Code" id="membership_code" class="form-control" value="">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <button type="button" class="btn btn-default" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i> Cancel</button>
                        </div>
                        <div class="col-6">
                            <input type="hidden" id="user_token" value="{{Session::get('token')}}">
                            <button type="button" class="btn btn-default pull-right btn-dark" id="redeem_button" onClick="redeemMembership()"> <i class="fa fa-check-circle"></i> Submit</button>
                        </div>
                    </div>
                    <div class="row text-danger" id="errors">

                    </div>
                    <div class="row text-succes" id="success">

                    </div>
                    <hr/>
                {{Form::open(array('url'=>'buy/membership'))}}
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <input type="hidden" name="membership_id" id="membership_id">
                                <input type="hidden" name="membership_price" id="membership_price">
                                <input type="hidden" name="city_id" id="city_id">
                                <input type="hidden" name="quantity" id="quantity" value="1">
                                <input type="hidden" name="is_coin_used" id="is_coin_used" value="0">
                                <button type="submit" class="btn btn-primary mb-0" > Pay <i class="fa fa-rupee"></i><span id="payable_membership_price"></span> </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
