<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Club JB - Internet Marketing &amp; Media Promotion Company.</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('css/select2.min.css')}}" rel="stylesheet">
    <link href="{{url('css/sidebar.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{url('css/style.css')}}" rel="stylesheet">
    <style>
        .red-border{
            border: 1px solid red;
        }
    </style>
</head>
<body class="clr-grey">
<div class="position-fixed">
    <div class="topheader-content mb-0 pad-b-03">
        <div class="row">
            <div class="col-12 pad-0">
                <div class="container">
                    <div class="row">
                        <div class="col-4 pr-0">
                            <h5 class="mar-0 top-logo">
                                <img src="images/logo-top.png" alt="" class=""></h5>
                        </div>
                        <div class="col-4 pad-0 text-center location-select">
                            <a href="#" data-toggle="modal" data-target="#location-change">{{$cities[$user->city_id]}} <i class="fa fa-sort-down"></i></a>
                        </div>
                        <div class="col-4 pl-0">
                            <div class="coins-txt text-right">
                                <img src="images/jb_coins.png" alt="">
                                <p>Coins
                                    <span>0</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@yield('content')
<div class="modal fade login-modal" id="location-change" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"><i class="fa fa-map-marker fa-lg"></i> Select Your Location</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{Form::open(array('url'=>'update_user', 'class'=>'login-form', 'id'=>'model_city'))}}
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            {{Form::hidden('country_id', $user->country_id, array('id'=>'model_country'))}}
                            {{Form::select('state_id', $states, $user->state_id, array('class'=>'form-control select2', 'data-placeholder'=>'Select State', 'id'=>'model_states'))}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            {{Form::select('city_id', $cities, $user->city_id, array('class'=>'form-control select2', 'data-placeholder'=>'Select City', 'id'=>'model_cities'))}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary btn-block mb-0 mt-0" onclick="submitCity()"> Proceed</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- SIDEBAR -->
<div data-sidebar="true" class="sidebar-trigger">
    <a class="sidebar-toggle text-black-50" href="#">
        <span class="sr-only">Sidebar toggle</span>
        <i class="fa fa-sidebar-toggle"></i>
    </a>
    <div class="sidebar-wrapper sidebar-default">
        <div class="sidebar-scroller">
            <div class="sidebar-profile">
                <div class="row">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <div class="col-3 pl-5">
                                    <div class="profile-pic text-center pad-0">
                                        <i class="fa fa-user-circle-o fa-3x"></i>
                                    </div>
                                </div>
                                <div class="col-9 pl-5 pr-5">
                                    <h5 class="mb-0">{{$user->name}} <a href="#"><i class="fa fa-pencil pull-right"></i></a></h5>
                                    <p>{{$user->phone_number}}</p>
                                    <div class="refeeral-bg">
                                        <i class="fa fa-users"></i> Total Referrals : {{$user->reference_id}}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="sidebar-menu">
                <li><a href="#"><i class="fa fa-user"></i> My Profile</a></li>
                <li><a href="#"><i class="fa fa-rupee"></i> My JB Coins</a></li>
                <li><a href="#"><i class="fa fa-file"></i> My Order</a></li>
                <li><a href="#"><i class="fa fa-heart-o"></i> My Favourite</a></li>
                <li><a href="#"><i class="fa fa-question-circle"></i> Help</a></li>
                <li><a href="#"><i class="fa fa-share-alt"></i> Share</a></li>
                <li><a href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i class="fa fa-power-off"></i> Logout</a></li>
                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/select2.full.min.js')}}"></script>
<script src="{{url('js/hammer.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/sidebar.js')}}"></script>
<script src="{{url('js/scripts.js')}}"></script>
@yield('footerScripts')
<script>

    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2({
            dropdownParent: $('#location-change')
        })
    });
    $(window).load(function(){
        if($('#model_cities').val() == null){
            $('#location-change').modal('show');
        }
    });
    $(document).on("change","#model_states",{target : $("#model_cities")},fetchCities);
</script>
</body>

</html>