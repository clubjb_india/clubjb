@extends('shared.master')
@section('content')
    <div class="position-fixed">
        <div class="topheader-content mb-0 pad-b-03">
            <div class="row">
                <div class="col-12 pad-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 pr-5 pl-5">
                                <h6>
                                    <a href="{{ url('store_dealgroups/'.$membershipId.'/'.$storeId) }}"> <i class="fa fa-arrow-left"></i></a> {{getDealGroupName($cat_type)}} ({{count($deals->data)}})
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="deals-listview">
        <div class="container pl-5 pr-5">
           @foreach($deals->data as $deal)
            <div class="deal-list-box">
                <a href="{{url('deals/details/'.$membershipId.'/'.$storeId.'/'.$cat_type.'/'.$deal->id)}}">
                    <div class="row">
                        <div class="col-4 pr-5">
                            <img src="{{$deal->store_image}}" alt="" class="img-thumbnail">
                        </div>
                        <div class="col-8 pl-5">
                            <div class="row">
                                <div class="col-12">
                                    <p class="deal-name">{{$deal->deal_name}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="price-txt">
                                        <p><b class="short-txt">Actual Deal Price</b> <span>{{$deal->actual_price}}/-</span> <i class="fa fa-rupee"></i>{{$deal->final_price}}/-</p>
                                    </div>
                                </div>
                                @if($purchased == true)
                                        <div class="col-6">
                                            <div class="buynow-txt buy-btn">
                                                <button  class="btn btn-primary" > Buy Now</button>
                                            </div>
                                        </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-7 pr-5">
                            <div class="terms-bg">
                                <span class="qty-txt">
                                    QTY:- {{$deal->quantity}}
                                </span>
                                <a onClick='openTermsModal("{{$deal->id}}")'>T&amp;C</a>
                                <a href="#" data-toggle="modal" data-target="#help-txt"><i class="fa fa-question-circle text-black-50"></i></a>
                                <a href="#">Deal Type</a>
                            </div>
                        </div>
                        <div class="d-none" id="{{$deal->id}}">
                            {!! $deal->term_condition !!}
                        </div>
                        <div class="col-5 pl-5">
                            <div class="buynow-txt">
                                <span>Savings upto</span> @if($deal->discount_type == 1)<i class="fa fa-rupee"></i> {{$deal->discount_price}}/- @elseif($deal->discount_type == 2) {{$deal->discount_price}}%@endif
                            </div>
                        </div>
                    </div>
                </a>
            </div>
           @endforeach
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade login-modal" id="term-conditions" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Terms and Conditions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="termContent">
                 </div>
            </div>
        </div>
    </div>

@endsection

@section('footerScripts')
    <script>
       // var URL ='http://newjb.clubjb.com/api/v1/';
        function openTermsModal( id){
            var termContent = $('#'+id).html();
            $('#termContent').html(termContent);
            $('#term-conditions').modal('show');
        }

        function viewDetails(url){
            window.location.href = url;
        }
    </script>
@endsection