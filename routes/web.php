
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/setFlash', 'MembershipController@setFlash');
Route::get('/getFlash', 'MembershipController@getFlash');

Route::get('/', 'MembershipController@membershipPriceGroups');
Route::get('register', 'MembershipController@register');
Route::get('verify_otp', 'MembershipController@verify_otp');
//Route::post('register', 'MembershipController@register_and_login');
Route::post('logout', 'MembershipController@logout')->name('logout');

//Route::group(['middlewareGroups' => 'web'], function () {
    Route::get('membership-price-group', 'MembershipController@membershipPriceGroups');
    Route::get('memberships', 'MembershipController@memberships');
    Route::get('dashboard', 'MembershipController@dashboard');
    Route::get('store', 'MembershipController@store');

    Route::post('update_user', 'MembershipController@update_user');
    Route::get('location', 'MembershipController@location');
    Route::get('/membership-list/{price}', 'MembershipController@membershipList');
    Route::get('/membership-categories/{membershipId}', 'MembershipController@storeCategories');
    Route::get('/membership-stores/{membershipId}/{categoryId}', 'MembershipController@membershipStores');
    Route::get('/store_dealgroups/{membershipId}/{storeId}', 'MembershipController@storeDealGroup');
    Route::get('/store_deal_bycategory/{membershipId}/{store_id}/{cat_type}', 'MembershipController@storeDealByCategory');
    Route::get('/deals/details/{membershipId}/{store_id}/{cat_type}/{dealId}', 'MembershipController@dealDetails');
    Route::post('cart/add', 'CartController@addToCart');
//Route::get('cart/add', 'CartController@addToCart');
    Route::post('buy/membership', 'PaymentController@orderMembership');




/*Route::get('/', function () {
    return view('sample.dashboard');

});
Route::get('/login', function () {
    return view('sample.login');

});

Route::get('/membership', function () {
    return view('sample.membership');

});
Route::get('/membership-list', function () {
    return view('sample.membership-list');

});
Route::get('/membership-store', function () {
    return view('sample.membership-store');

});
Route::get('/deals-group', function () {
    return view('sample.deals-group');

});
Route::get('/membershipdeals', function () {
    return view('sample.membership-deals');

});
Route::get('/membershipcategories', function () {
    return view('sample.membership-categories');

});
Route::get('/dealbuy', function () {
    return view('sample.deal-buy');

});
Route::get('/paymentfailure', function () {
    return view('sample.payment-failure');

});
Route::get('/securityissue', function () {
    return view('sample.security-issue');

});
Route::get('/paymentsuccessfull', function () {
    return view('sample.succesfull');

});
Route::get('/loginsignup', function () {
    return view('sample.login-signup');

});*/
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/phpinfo', function () {
    echo phpinfo();

});